#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Perform TL and gradient tests
Created on Fri Jul 12 13:16:27 2019

@author: alisonfowler
"""

from parameters import *
import DataAssimilation as DA
import numpy as np
from numpy import linalg as LA
import matplotlib.pyplot as plt
import Lorenz96
from scipy.linalg import pinv
import math

'''
Test whether the TL of the model has been coded correctly
lim(gamma->0)(||m(x+gamma*dx)-m(x)-M(x)*gamma*dx||)(M(x)*gamma*dx)=0
where m is the NL model, M is the TL model
'''
def test_TL(dirn):
    print('-------Testing TL code--------')
    nsteps=5     # Number of steps, comparable to assimilation window length
    pert= True_ics()#
    
    gamma=10.
    
    x0=True_ics()
    ''' propogate unperturbed ics'''
    unperturbed_traj = \
    Lorenz96.Lorenz96_nonLinear_propogation(x0,nsteps,F_A)
    
    n_pert=5
    rel_err=np.empty([n_pert,])
    gamma_vec=np.empty([n_pert,])
    for i in range(n_pert):
        gamma=gamma/10.
        x0_p=gamma*pert
        x02=x0+x0_p #perturbed initial conditions
        ''' propogate perturbed ics'''
        perturbed_traj = \
        Lorenz96.Lorenz96_nonLinear_propogation(x02,nsteps,F_A)
        NL_pert = perturbed_traj-unperturbed_traj
        ''' propogate perturbations using TL'''
        #print('unperturbed_traj')
        #print(unperturbed_traj)
        TL_pert = Lorenz96.Lorenz96_TL_propogation(x0_p,nsteps,x0,F_A)
        diff = NL_pert[:,nsteps-1]-TL_pert[:,nsteps-1]
        rel_err[i] = 100 * LA.norm(diff) / LA.norm(TL_pert[:,nsteps-1])
        gamma_vec[i] = gamma
        print('norm(NL_pert[:,nsteps]-TL_pert[:,nsteps]),norm(TL_pert[:,nsteps],rel_err[i]')
        print(LA.norm(diff),LA.norm(TL_pert[:,nsteps-1]),rel_err[i])

    plt.figure(1)
    plt.loglog(gamma_vec,rel_err)
    plt.xlabel('gamma')
    plt.ylabel('Relative error')
    plt.title('TL test')
    plt.show()
    plt.savefig(dirn+'/TL_test.png')
    
    '''
    plt.figure(2)
    plt.subplot(2,1,1)
    plt.plot(unperturbed_traj[:,nsteps-1], label='unperturbed trajectory')
    plt.plot(perturbed_traj[:,nsteps-1], label='perturbed trajectory')
    plt.legend()
    plt.title('gamma='+str(gamma))
    plt.subplot(2,1,2)
    plt.plot(-unperturbed_traj[:,nsteps-1]+perturbed_traj[:,nsteps-1], label='f(x_0+dx_0)-f(x_0)')
    plt.plot(TL_pert[:,nsteps-1], label='F(dx_0)')
    plt.legend()
    plt.show()
    '''

'''
Test whether the ADJOINT of the model has been coded correctly
<Mdx,Mdx>=<M'Mdx,dx>
''' 
def test_ADJ():    
    print('-------Testing ADJ code--------')
    nsteps=5     # Number of steps, comparable to assimilation window length
    pert= np.ones([Nx,]) #True_ics/10.#
    
    x=True_ics()
    ''' propogate unperturbed ics'''
    unperturbed_traj = \
    Lorenz96.Lorenz96_nonLinear_propogation(x,nsteps,F_A)  
    ''' propogate perturbations using TL'''
    TL_pert = Lorenz96.Lorenz96_TL_propogation(pert,nsteps,x,F_A)

    x_hat=np.empty(TL_pert.shape)
    print('x_hat.shape',x_hat.shape)
    x_hat[:,nsteps]=TL_pert[:,nsteps]
    for adj_loop in np.arange(nsteps,0,-1):
        print('adj_loop',adj_loop)
        x_hat[:,adj_loop-1] = \
        Lorenz96.Lorenz96_ADJ_propogation(x_hat[:,adj_loop],unperturbed_traj[:,adj_loop-1],F_A)
    
    Forward_prod = np.dot(TL_pert[:,nsteps],TL_pert[:,nsteps])
    #print('TL_pert[:,nsteps+1]',TL_pert[:,nsteps+1])
    
    Adj_prod = np.dot(x_hat[:,0],TL_pert[:,0])
    #print('pert',pert)
    #print('TL_pert[:,0]',TL_pert[:,0])
    print('x_hat[:,0]',x_hat[:,0])
    Diff = Forward_prod - Adj_prod
    
    print('Forward_prod',Forward_prod)
    print('Adj_prod',Adj_prod)
    print('Diff',Diff)

       
'''
Test the gradient of the cost function
'''
def test_gradinc():
   nsteps = 2 # number of time steps
   length = 16
   x = np.zeros(Nx,nsteps+1)
   alpha_vec=np.zeros(length,1)
   fn_vec = np.zeros(length,1)
   resid_vec = np.zeros(length,1)
   
   x[:,0] =  True_ics
   u_bg = True_ics + np.random.normal(0, 1, Nx)


def test_grad(x, arg):
    """Perform gradient test and plot the verification of gradient calculation (phi(alpha)) and the variation of
    residual (phi(alpha) - 1). x is the state of size Nx (or Nx + Nb if using BC) and arg are the arguments defined in
    DataAssimilation -> var4d. """
    print("-------Testing Gradient-------")
    alpha = np.array(
        [10 ** (-15), 10 ** (-14), 10 ** (-13), 10 ** (-12), 10 ** (-11), 10 ** (-10), 10 ** (-9), 10 ** (-8),
         10 ** (-7),
         10 ** (-6), 10 ** (-5), 10 ** (-4), 10 ** (-3), 10 ** (-2), 10 ** (-1), 10 ** 0])  # Alpha varies from 0 to 1.
    ###Calculate the gradient test###
    # print("x =", x)
    J = DA.calc_J_inc(x, arg)  # Calculate the cost function using the given state
    dJ = DA.calc_gradJ(x, arg)  # Calculate the gradient of the cost function using the given state.
    normdJ = LA.norm(dJ)  # Calculate the norm of the gradient.
    # print("J =", J)
    # print("dJ =", dJ)
    # print("normdJ =", normdJ)
    # Initialise h as a vector of size of the state.
    h = np.zeros(len(dJ))
    # Define h to be of unit length, such that for each spatial element, h=gradJ/norm(gradJ)
    for i in range(len(dJ)):
        h[i] = np.divide(dJ[i], normdJ)
    # print("h =", h)
    hTdJ = h.T.dot(dJ)  # Calculate h^T*gradJ - a scalar to be used to expand J

    # Initialise Jxplusa s.t it can hold all values of alpha.
    Jxplusa = np.zeros(len(alpha))
    # Initialise numerator and denominator so that they can hold all values of alpha
    num = np.zeros(len(alpha))
    den = np.zeros(len(alpha))
    # Initialise phi s.t it can hold all values of alpha
    phi = np.zeros(len(alpha))
    # print("shape of h =", np.shape(h))
    # print("shape of dJ =", np.shape(dJ))
    # print("shape of hTdJ =", np.shape(hTdJ))
    # print("x =", x)
    # print("hTdJ = ", hTdJ)
    # print("x + hTdJ =", x + hTdJ)
    for i in range(len(alpha)):  # for each i in alpha
        # Expand J:
        Jxplusa[i] = DA.calc_J_inc(x + alpha[i] * h, arg)
        # Define phi to be the gradient test, as a function of alpha.
        # Phi is a fraction, define numerator and denominator separately.
        num[i] = np.subtract(Jxplusa[i], J)
        den[i] = alpha[i] * hTdJ
        phi[i] = np.divide(num[i], den[i])
        # print("Jxplusa =", Jxplusa[i])
        # print("num =", num)
        # print("den =", den[i])
        # print("phi =", phi[i])
    # Plot phi(alpha) with alpha on a log scale.
    plt.plot(alpha, phi)
    plt.xscale("log")
    plt.xlabel("alpha")
    plt.ylabel("$\phi$($alpha$)")
    plt.show()
    # Calculate variation in residual
    # print("phi =", phi)
    res = abs(phi - 1)
    # print("res =", res)
    # Plot phi(alpha) - 1
    plt.plot(alpha, res)
    plt.xscale("log")
    plt.yscale("log")
    plt.xlabel("alpha")
    plt.ylabel("|$\phi$($alpha$) - 1|")
    plt.show()

# J = {0: [1, 1, 1]}
# gradJ = {0: [[2, 4, 2, 1], [2, 4, 2, 1], [2, 4, 2, 1]]}
# normdJ = {0: [5, 5, 5]}
# test_grad(J, gradJ, normdJ)


      
