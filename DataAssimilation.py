#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
4DVar routines to compute analysis and forecasts
Created on Thu Jul  4 12:48:55 2019

@author: alisonfowler
"""
from parameters import (outer_loops, N_ob_types, L_a, F_A, Nx, Nb, L_f,
                        Obs_sep_dist, xtol, max_it, true_b, sigma_bbeta, Ne, Ics, F, dt)
import numpy as np
import Lorenz96
from scipy.linalg import pinv
from scipy.optimize import minimize
import matplotlib.pyplot as plt
import math
from numpy import linalg as LA

##############################################################################
# 4DVar
##############################################################################


def var4d_cycled(bginfo,obinfo,verbosity, BC, N_c, sigma_o, dom_size, b_lower, b_upper, n_lower, WC=0):
    """Data assimilation routine for Lorenz 1996 using 4DVar.
    Inputs:  - bg_info, first guess at beginning of cycle and B matrix
             - obinfo- contains ob values, H, R and times for different observation types
    Outputs: - x_a, the analysis"""
    ''' initialise analysis with the background estimate'''
    xb = bginfo['fg']  # .T  # Includes both x and beta
    ana_time = 0  # time index of analysis
    
    '''find all times when observations are available'''
    ob_times_all = np.array([], dtype=int)
    '''Initialise arrays to hold all state variables (including bias parameters) for analysis and bkd trajectory'''
    Xa_alltimes = np.zeros([dom_size, (L_a + 1) * N_c])
    fg_traj = np.zeros([dom_size, (L_a + 1) * N_c + L_f])
    Ny_max = 0
    Ny_alltimes = np.zeros([N_c*L_a + 1, ])
    for ob_type_index in range(N_ob_types):
        ob_times_all = np.append(ob_times_all, obinfo[ob_type_index]['ob_times'])
        Ny_max = Ny_max+obinfo[ob_type_index]['Ny'] 
        Ny_alltimes[obinfo[ob_type_index]['ob_times']] =\
            Ny_alltimes[obinfo[ob_type_index]['ob_times']] + obinfo[ob_type_index]['Ny'] 
    del ob_type_index
    unique_ob_times = np.unique(ob_times_all)
    ''' Loop over assimilation cycle'''
    J = {}
    dJ = {}
    normdJ = {}
    Ny_window = np.empty([N_c, ])
    ob_info_t = {}
    k = 0  # Used to shift Xa_alltimes
    for cycle_index in range(N_c):
        AW_end_time = ana_time + L_a  # end time of current analysis window
        if verbosity:
            print('***************')
            print('WINDOW start', ana_time, ', end', AW_end_time)
        if cycle_index < N_c:
            ''' propagate background to end of next assimilation cycle '''
            if WC:
                # Propagate both state and model bias estimate forward
                prop = Lorenz96.Lorenz96_nonLinear_propogation(xb.flat[0:Nx], L_a, F_A, xb.flat[n_lower: dom_size], WC)
            else:
                prop = Lorenz96.Lorenz96_nonLinear_propogation(xb.flat[0:Nx], L_a, F_A, WC)  # eta will be set to 0
            xb_prop = prop[0]
            if BC:  # If using bias correction, include beta in xb propagation.
                xb_prop = np.append(xb_prop, xb.flat[b_lower:b_upper]*np.ones((1, L_a+1)), axis=0)
            if WC:  # If using weak-constraint 4DVar include eta in xb propagation
                xb_prop = np.append(xb_prop, prop[1], axis=0)
            fg_traj[:, ana_time:AW_end_time + 1] = xb_prop
        tmp = np.array([], dtype=int);UOT_thiscycle=np.array([], dtype=int)
        ''' Get unique ob times relevant to window'''
        tmp = unique_ob_times[unique_ob_times > ana_time-1]
        UOT_thiscycle = tmp[tmp<AW_end_time+1]  # unique observation times for this cycle
        ''' total number of observations in assimilation window '''
        Ny_window[cycle_index] = np.sum(Ny_alltimes[UOT_thiscycle])
        bginfo['fg_traj'] = fg_traj
        true = Lorenz96.Lorenz96_nonLinear_propogation(Ics(), L_a, F)
        Xa_traj, J[cycle_index], dJ[cycle_index], normdJ[cycle_index], Xa_0, arg = \
            var4d(ana_time, AW_end_time, bginfo,obinfo,UOT_thiscycle, Ny_max, xb, verbosity, BC, sigma_o, ob_info_t,
                  dom_size, b_lower, b_upper, n_lower, WC)
        an_beginning = ana_time + k
        an_end = AW_end_time + k
        Xa_alltimes[:, an_beginning:an_end+1] = Xa_traj
        k += 1
        print("Xa_traj =", Xa_traj[0, :])
        ana_time = AW_end_time  # analysis time for next assim window
        if cycle_index < N_c-1:
            xb = Xa_traj[:, L_a]  # background for next assim window for state, this is for x and beta.
        else:
            xb = xb_prop[:, L_a]
    del cycle_index

    # At the end of each cycle, use a new B for the next cycle based on the current analysis.
        
    ''' propagate background to end of forecast time '''
    # print("shape of xb =", np.shape(xb))
    if WC:  # if using weak-constraint, propagate both state and eta
        traj = Lorenz96.Lorenz96_nonLinear_propogation(np.squeeze(xb)[0:Nx], L_f,F_A, np.squeeze(xb)[n_lower: dom_size],
                                                       WC)
    else:  # if not using weak-constraint, eta will be set to 0 automatically
        traj = Lorenz96.Lorenz96_nonLinear_propogation(np.squeeze(xb)[0:Nx], L_f,F_A, WC)
    fg_traj[0:Nx, ana_time:ana_time + L_f - 1] = traj[0][:, 1:L_f]
    if BC:  # If using bias correction, propagate the background bias forward.
        fg_traj[b_lower: b_upper, ana_time:ana_time + L_f] = np.squeeze(xb)[b_lower:b_upper]*np.ones(L_f)
    if WC:
        fg_traj[n_lower: dom_size, ana_time:ana_time + L_f -1] = traj[1][:, 1:L_f]
    '''add to dictionary'''
    bginfo['fg_traj'] = fg_traj
    print("Xa_alltimes =", Xa_alltimes[0, :])
    return Xa_alltimes, J, dJ, normdJ, Ny_window, Xa_0, arg
        
        
def var4d(ana_time, AW_end_time,  bginfo, obinfo, UOT_thiscycle, Ny_max, x_0, verbosity, BC, sigma_o, ob_info_t,
          dom_size, b_lower, b_upper, n_lower, WC):
    """Performs minimisation of cost function for one observation time"""
    
    J = np.empty([1, outer_loops])
    dJ = np.empty([dom_size, outer_loops])
    normdJ = np.empty([1, outer_loops])
    for i_outer in range(outer_loops):
        
        if verbosity:
            print('*** OUTER LOOP ', i_outer+1, '***')
    
        ''' run background trajectory throughout assimilation window using 
        assimilation model (forcing)'''
        if WC:  # if using weak-constraint, propagate both state and model bias estimate
            traj = Lorenz96.Lorenz96_nonLinear_propogation(np.squeeze(x_0)[0:Nx], L_a, F_A,
                                                           np.squeeze(x_0)[n_lower: dom_size], WC)
        else:  # if not using weak-constraint, eta is automatically set to 0
            traj = Lorenz96.Lorenz96_nonLinear_propogation(np.squeeze(x_0)[0:Nx], L_a, F_A, WC)
        x0_traj = traj[0]
        if BC:  # If using bias correction, include beta in x0_traj
            x0_traj = np.append(x0_traj, np.squeeze(x_0)[b_lower:b_upper]*np.ones((1, L_a + 1)), axis=0)
        if WC:  # If using weak-constraint
            x0_traj = np.append(x0_traj, traj[1], axis=0)
    
        ''' Calculate innovations '''
        # ob_info_t = {}  Moving this to outside the for loop so that each cycle is stored.
        for ob_time in UOT_thiscycle:
            model_time_thiscycle = ob_time - ana_time
            innov = np.array([])
            R_diag = np.array([])
            H_obtime = np.array([])
            i=0
            for ob_type_index in range(N_ob_types):
                if ob_time in obinfo[ob_type_index]['ob_times']:
                    i=i+1
                    Ny = obinfo[ob_type_index]['Ny']
                    ob_time_index = np.argmin(abs(obinfo[ob_type_index]['ob_times']-ob_time))
                    H = obinfo[ob_type_index]['H']
                    ob = obinfo[ob_type_index]['obs'][:, ob_time_index]
                    if BC: 
                        innov = np.append(innov, (np.dot(H, x0_traj[0:b_upper, model_time_thiscycle]) - ob))
                    else:  
                        innov = np.append(innov, (np.dot(H, x0_traj[0:Nx, model_time_thiscycle]) - ob))
                    R_diag = np.append(R_diag,sigma_o[ob_type_index]*np.ones(Ny))
                    if i == 1:
                        H_obtime = H
                    else:
                        H_obtime = np.concatenate((H_obtime, H))
            del ob_type_index
            R = np.diag(R_diag)
            Rinv = pinv(R)
            ob_info_t[ob_time] = {'innov': innov, 'Rinv': Rinv, 'H': H_obtime}
        ''' initialise perturbations'''
        X0_p = np.zeros(dom_size)
        ''' Calculate  background innovations'''
        X0_b = bginfo['fg_traj'][:, ana_time].T-x_0  # Dependent on x and beta
        
        if verbosity:
            ''' minimise cost function'''
            print('--- minimising cost function ---')
        arg = [X0_b, x0_traj, bginfo, UOT_thiscycle, ana_time, ob_info_t, BC, dom_size, b_lower, b_upper, n_lower, WC]
        res = minimize(calc_J_inc,
                      X0_p,
                      args = arg, 
                      jac=calc_gradJ,
                      method='CG',
                      tol = xtol,
                      options={'disp': verbosity, 'maxiter': max_it})  # method='CG' original, SLSQP
        '''update estimate of the state'''
        Xp = res['x']
        N_iter = res['nit']
        x_0 = x_0 + Xp
        J[:, i_outer] = res['fun']
        dJ[:, i_outer] = res['jac']
    del i_outer
    Xa_0 = x_0  # Dependent on x and beta and eta
    ''' propagate analysis to end of  assimilation cycle '''
    if WC:
        # if using weak-constraint, propagate both state and model bias estimate together
        traj = Lorenz96.Lorenz96_nonLinear_propogation(np.squeeze(Xa_0)[0:Nx], L_a, F_A,
                                                          np.squeeze(Xa_0)[n_lower: dom_size], WC)
    else:  # if not using weak-constraint eta is automatically set to 0.
        traj = Lorenz96.Lorenz96_nonLinear_propogation(np.squeeze(Xa_0)[0:Nx], L_a, F_A, WC)
    Xa_traj = traj[0]
    if BC:  # If using bias correction, propagate bias analysis separately
        Xa_traj = np.append(Xa_traj, np.squeeze(Xa_0)[b_lower:b_upper]*np.ones((1, L_a + 1)), axis=0)
    if WC:
        Xa_traj = np.append(Xa_traj, traj[1], axis=0)
    return Xa_traj, J, dJ, normdJ, Xa_0, arg


def calc_J_inc(X0_p, arg):
    '''Calculate cost function'''
    X0_b = arg[0]  # Dependent on x and beta and eta
    x0_traj = arg[1]  # Dependent on x and beta and eta
    bginfo = arg[2]  # Dependent on x and beta and eta
    UOT_thiscycle = arg[3]
    ana_time = arg[4]
    ob_info_t = arg[5]
    BC = arg[6]
    dom_size = arg[7]
    b_lower = arg[8]
    b_upper = arg[9]
    n_lower = arg[10]
    WC = arg[11]
    f = 0  # initialise cost function
    X_p = np.zeros([dom_size, L_a + 1])  # initialise Perturbations
    X_p[:, 0] = X0_p
    ''' propagate perturbations throughout window using TL'''
    if WC:  # if using weak-constraint, propagate both state and model bias forward
        prop = Lorenz96.Lorenz96_TL_propogation(X_p[0:Nx, 0], L_a, x0_traj[0:Nx, 0], F_A, X_p[n_lower: dom_size, 0], x0_traj[n_lower:dom_size, 0], WC)
    else:  # if not using weak-constraint, etap0 = eta0 = 0 and is not used.
        prop = Lorenz96.Lorenz96_TL_propogation(X_p[0:Nx, 0], L_a, x0_traj[0:Nx, 0], F_A, 0, 0, WC)
    X_p[0:Nx, :] = prop[0]  # state propagation
    if BC:  # Propagate bias correction coefficient perturbations throughout window
        X_p[b_lower:b_upper, :] = np.squeeze(X0_p)[b_lower:b_upper] * np.ones(L_a + 1)
    if WC:
        X_p[n_lower:dom_size, :] = prop[1]  # model bias estimate of propagation
    '''
    Calculate cost function
    '''
    '''Background term'''
    db = (X0_p - X0_b).T
    f = f + 0.5 * np.matmul(np.matmul(db.T, bginfo['invB']), db)
    '''Observation term'''
    for ob_time in UOT_thiscycle:
        model_time_thiscycle = ob_time - ana_time
        if BC: 
            dy = ob_info_t[ob_time]['innov'] + np.matmul(ob_info_t[ob_time]['H'], X_p[0:Nx + Nb, model_time_thiscycle])
        else: 
            dy = ob_info_t[ob_time]['innov'] + np.matmul(ob_info_t[ob_time]['H'], X_p[0:Nx, model_time_thiscycle])
        f = f + 0.5 * np.dot(np.dot(dy.T, ob_info_t[ob_time]['Rinv']), dy)
    del ob_time
    return f

     
def calc_gradJ(X0_p, arg):
    ''' calculate gradient of cost function'''
    X0_b =arg[0]  # Dependent on x and beta
    x0_traj = arg[1]  # Dependent on x and beta
    bginfo = arg[2]  # Dependent on x and beta
    UOT_thiscycle = arg[3]
    ana_time = arg[4]
    ob_info_t = arg[5]
    BC = arg[6]
    dom_size = arg[7]
    b_lower = arg[8]
    b_upper = arg[9]
    n_lower = arg[10]
    WC = arg[11]
    X_hat = np.zeros([dom_size, L_a + 1])
    X_p = np.zeros([dom_size, L_a + 1])  # initialise Perturbations
    X_p[:, 0] = X0_p
    ''' propagate initial perturbation to end of assimilation window using TL'''
    if WC:
        # if using weak-constraint, propagate both state and model bias estimate together
        prop = Lorenz96.Lorenz96_TL_propogation(X_p[0:Nx, 0], L_a, x0_traj[0:Nx, 0], F_A, X_p[n_lower:dom_size, 0], x0_traj[n_lower:dom_size, 0], WC)
    else:
        # if not using weak-constraint, set etap0 = eta0 = 0
        prop = Lorenz96.Lorenz96_TL_propogation(X_p[0:Nx, 0], L_a, x0_traj[0:Nx, 0], F_A, 0, 0, WC)
    X_p[0:Nx, :] = prop[0]
    if BC:
        X_p[b_lower:b_upper, :] = X_p[b_lower:b_upper, 0]*np.ones(L_a + 1)  # If using bias correction, propagate beta
    #     # print("X_p in gradJ after append =", X_p)
    if WC:
        X_p[n_lower:dom_size, :] = prop[1]  # If using weak-constraint, propagate eta
    
    AW_end_time = ana_time + L_a
    '''Observation term'''
    if AW_end_time in UOT_thiscycle:
        if WC: 
            Hdx = np.matmul(ob_info_t[AW_end_time]['H'], X_p[0:dom_size - Ne, L_a])
        else: 
            Hdx = np.matmul(ob_info_t[AW_end_time]['H'], X_p[:, L_a])
        dy = (ob_info_t[AW_end_time]['innov'] + Hdx)
        HRinv = np.matmul(ob_info_t[AW_end_time]['H'].T, ob_info_t[AW_end_time]['Rinv'])
        if WC: 
            X_hat[0:dom_size - Ne, L_a] = np.matmul(HRinv, dy)
        else: 
            X_hat[:, L_a] = np.matmul(HRinv, dy)
    for adj_loop in np.arange(L_a-1, -1, -1):
        ''' propagate adjoint variable back in time'''
        if WC:  # if using weak-constraint, use adjoint to calculate both state and model bias throughout window
            full_hat = Lorenz96.Lorenz96_ADJ_propogation(X_hat[0:Nx, adj_loop + 1], x0_traj[0:Nx, adj_loop], F_A,
                                                         X_hat[n_lower:dom_size, adj_loop + 1], WC)
        else:  # if not using weak-constraint, set eta_hat to 0, it is not used in adjoint propagation
            full_hat = Lorenz96.Lorenz96_ADJ_propogation(X_hat[0:Nx, adj_loop + 1], x0_traj[0:Nx, adj_loop], F_A, 0, WC)
        X_hat[0:Nx, adj_loop] = full_hat[0]  # state calculated by adjoint throughout window
        if BC:
            X_hat[b_lower:b_upper, adj_loop] = X_hat[b_lower:b_upper, adj_loop + 1]  # beta is constant in window
        if WC:
            X_hat[n_lower:dom_size, adj_loop] = full_hat[1]  # eta calculated by adjoint throughout window
        ob_time = ana_time + adj_loop
        ''' if observations available at time step add on influence'''
        if ob_time in UOT_thiscycle:
            if BC: 
                Hdx = np.matmul(ob_info_t[ob_time]['H'], X_p[0:Nx + Nb, adj_loop])
            else: 
                Hdx = np.matmul(ob_info_t[ob_time]['H'], X_p[0:Nx, adj_loop])
            dy = (ob_info_t[ob_time]['innov'] + Hdx)
            # print("shape of dy =", np.shape(dy))
            HRinv = np.matmul(ob_info_t[ob_time]['H'].T, ob_info_t[ob_time]['Rinv'])
            if BC: 
                X_hat[0:Nx + Nb, adj_loop] = (X_hat[0:Nx + Nb, adj_loop] + np.matmul(HRinv, dy))
            else: 
                X_hat[0:Nx, adj_loop] = (X_hat[0:Nx, adj_loop] + np.matmul(HRinv, dy))
    del adj_loop
    '''Background term'''
    db = (X0_p - X0_b).T
    X_hat[:, 0] = X_hat[:, 0] + (np.matmul(bginfo['invB'], db)).flat
    g = X_hat[:, 0]
    
    return g
