"""Calculate the analysis state over many realisations and plot the mean of these realisations, against the mean of the
observations and the truth."""

##################################################################
# GENERIC MODULES REQUIRED
##################################################################

import os
import errno
import sys
import numpy as np
import math
import matplotlib.pyplot as plt

##################################################################
# CUSTOM FUNCTIONS AND MODULES REQUIRED
##################################################################

from parameters import Ics, L_a, N_c, L_f, F, N_ob_types, Obs_sep_time, Nx, Nb, Ne, beta_o, Obs_sep_dist, Obs_kernal_width, F_A, true_b, dt
import Lorenz96
import generate_Data_Matrices
import DataAssimilation
import Tests

#import Plotting
#import Diagnostics

################################################################
# Optional parameters
#################################################################
#################################################################
# 1. generate truth
#################################################################
print('********** generating truth ************')
true_traj = Lorenz96.Lorenz96_nonLinear_propogation(Ics(),L_a*N_c+L_f-1,F)[0]
# print("shape of true =", np.shape(true_traj))
var_incr = 1  # increment between variables to plot
if L_a == 1 and N_c == 1:
    var_incr = 1  # If the size of the window is 1 (3DVar) and there is only one cycle then plot all spatial variables.
N = len(true_traj[:, 0])  # Number of spatial variables
# print("N =", N)
num_state_plot = math.ceil(N / var_incr)  # number of variables to plot
# WC = 0  # 1/0 Run/don't run weak-constraint 4DVar

def all_real(N_realisations, BC, sigma_b, sigma_o, beta_b_obs, L_b, B=0, WC=0):
    """Calculate analysis, background and observations for data assimilation system over N_realisations. """
    # Set domain size depending on which bias correction techniques are used.
    if WC:
        if BC:  # if using weak-constraint and VarBC
            dom_size = Nx + Nb + Ne
            b_lower = Nx  # lower bound of location of obs bias coefficient in state
            b_upper = Nx + Nb  # upper bound of location of obs bias coefficient in state
            n_lower = Nx + Nb  # lower bound of location of model bias parameter in state
        else:  # if using only weak-constraint
            dom_size = Nx + Ne
            b_lower = []  # lower bound on obs bias coeff does not exist
            b_upper = []  # upper bound on obs bias coeff does not exist
            n_lower = Nx  # lower bound of location of model bias parameter in state
    elif BC:  # if only using VarBC
        dom_size = Nx + Nb
        b_lower = Nx  # lower bound of location of model bias parameter in state
        b_upper = Nx + Nb  # upper bound of location of obs bias coefficient in state
        n_lower = []  # lower bound on model bias parameter does not exist
    else:  # if no bias correction techniques are used
        dom_size = Nx
        b_lower = []  # lower bound on obs bias coeff does not exist
        b_upper = []  # upper bound on obs bias coeff does not exist
        n_lower = []  # lower bound on model bias coeff does not exist
    # Initialise arrays to hold observations and analyses from all realisations.
    obs_ave = []  # Initialise obvservation array to hold average observation over all realisations.
    obs_total_ar = {}  # Initialise observation dictionary to hold all observations for each ob_type_index
    Ny = np.zeros(N_ob_types)  # Initialise number of observations in space to hold all observation types.
    total_no_obs_space = 0  # Initialise the total number of observations in space (from all obs types).
    for ob_type_index in range(N_ob_types):
        Ny[ob_type_index] = generate_Data_Matrices.generate_obs(true_traj, ob_type_index, 1, BC, 0, N_c, sigma_o, WC)['Ny']
        # Number of observations in space
        if L_a >= Obs_sep_time[0]:  # If using 3DVar over one time step or 4DVar, want multiple observations over window
            Nyt = math.ceil((L_a*N_c)/Obs_sep_time[ob_type_index])  # Number of observations in time
        else:  # If using 3DVar over multiple time steps, want observations at first time step of window only.
            Nyt = N_c
        obs_total_ar[ob_type_index] = np.zeros((N_realisations, int(Ny[ob_type_index]), Nyt))  # This changes the
        # original obs_total from a nested list to one array.
        total_no_obs_space += Ny[ob_type_index]  # Add up the total number of observations in space
    innov_total_ar = np.zeros((N_realisations, N_c, int(total_no_obs_space)))  # Initialise an array to hold all
    # innovation vectors for l
    state_a_total = np.zeros((N_realisations, Nx, (L_a + 1)*N_c+L_f))  # Initialise array to store all states
    # analyses from all realisations. This is over Nc + 1 cycles to be used for the initial cycle used for initialising B.
    state_b_total = np.zeros((N_realisations, Nx, (L_a + 1)*N_c+L_f))  # Initialise array to store all state backgrounds from
    # all realisations.
    if BC:   # If using bias correction
        bias_a_total = np.zeros((N_realisations, Nb, (L_a + 1)*N_c+L_f))  # Initialise array to store all bias coeff
        # analyses from all realisations.
        bias_b_total = np.zeros((N_realisations, Nb, (L_a + 1) * N_c + L_f))  # Initialise array to store all bias
        # coeff backgrounds from all realisations.
    if WC:  # If using model bias correction
        wc_a_total = np.zeros((N_realisations, Ne, (L_a + 1)*N_c+L_f))  # Initialise array to store all model bias
        # coeff analyses from all realisations.
        wc_b_total = np.zeros((N_realisations, Ne, (L_a+1) * N_c + L_f))  # Initialise array to store all bias
        # coeff backgrounds from all realisations.


    '''Use background error values calculated from ensemble to calculate the analysis over many realisations for the next
    N_c cycles.'''
    trigger = 0  # Used to decide whether to plot B or not - only plot B on one realisation, as B will be the same for all
    # realisations.
    for nr in range(N_realisations):  # Calculate analysis for each realisation
        if ((nr + 1) % 5) == 0:
            print('performing realisation ' + str(nr + 1) + ' of ' + str(N_realisations))

        #################################################################
        # 2. generate obs
        #################################################################
        # print('********** generating observations ************')
        Ob_info = [''] * N_ob_types
        # print("Ob_info =", Ob_info)
        for ob_type_index in range(N_ob_types):
            if beta_o[ob_type_index] == 0:
                # If ob_type is considered unbiased, do not use bias correction.
                print("###No bias in observations: if bias correcting, set bias correction to be zero.###")
                Ob_info[ob_type_index] = generate_Data_Matrices.generate_obs(true_traj, ob_type_index, 1, BC, 0, N_c, sigma_o, WC)
            else:  # If ob_type is biased, use bias correction when creating H
                print("###Bias in observations: if bias correcting, include bias correction in observation operator.###")
                Ob_info[ob_type_index] = generate_Data_Matrices.generate_obs(true_traj, ob_type_index, 1, BC, 1, N_c, sigma_o, WC)
            # print("Ob_info[ob_type_index]['obs'] =", Ob_info[ob_type_index]['obs'])
            obs_total_ar[ob_type_index][nr, :, :] = Ob_info[ob_type_index]['obs']
        #################################################################
        # 3. generate first guess
        #################################################################
        print('********** generating first guess ************')
        initial = 0  # Initial = 0 generates B from the initial ensemble, initial = 1 generates homogenous B from
        # covariances in parameters.
        bg_info = generate_Data_Matrices.generate_fg(1, BC, initial, trigger, sigma_b, Ob_info, beta_b_obs, L_b,
                                                     sigma_o, dom_size, b_lower, b_upper, n_lower, cycledB=B, WC=WC)
        # print("fg traj before 4DVar=", bg_info['fg_traj'][Nx:Nx + Ne, :])
        trigger = 0  # Don't plot B for future realisations.

        #################################################################
        # 4. perform DA
        #################################################################
        print('********** Performing cycled 4DVar ************')
        Xa_traj, J, dJ, normdJ, Ny_window, x, arg = DataAssimilation.var4d_cycled(bg_info,Ob_info,1, BC, N_c, sigma_o,
                                                                                dom_size, b_lower, b_upper, n_lower, WC)
        # Tests.test_grad(x, arg)  # Perform gradient test with cost function and its derivatives
        # print("fg traj after 4DVar=", bg_info['fg_traj'][Nx + Nb:Nx+Nb + Ne, :])
        # print("Xa_traj[beta] =", Xa_traj[Nx:Nx+Nb, :])
        # print("shape of Xa_traj=", np.shape(Xa_traj))
        # if N_c >= 2 and L_a>=Obs_sep_time[0]:  # When there are more than or equal to two cycles AND ob times is
        #     # less than assimilation window
        #     innov1 = arg[5][1]['innov']  # Innovation vector at second cycle
        #     innov0 = arg[5][0]['innov']  # Innovation vector at first cycle
        #################################################################
        # 5. generate forecast
        #################################################################
        if WC:  # if using weak-constraint, propagate both state and model bias estimate together
            full_fcst = Lorenz96.Lorenz96_nonLinear_propogation(Xa_traj[0:Nx, L_a*N_c], L_f, F_A, Xa_traj[n_lower:dom_size, L_a*N_c], WC)
        else:  # if not using weak-constraint, eta=0 automatically
            full_fcst = Lorenz96.Lorenz96_nonLinear_propogation(Xa_traj[0:Nx, L_a*N_c], L_f, F_A, WC)
        fcst = full_fcst[0]
        if BC:  # If using bias correction, generate bias forecast by propagating forward.
            fcst = np.append(fcst, Xa_traj[b_lower:b_upper,L_a*N_c]*np.ones((1, L_f+1)), axis=0)
        if WC:  # if using weak-constraint, generate model bias forecast from Lorenz96_nonLinear_propagation
            fcst = np.append(fcst, full_fcst[1], axis=0)
        analysis_fcst = np.concatenate((Xa_traj, fcst[:, 1:L_f + 1]), axis=1)  # Forecast for next L_f time steps.
        state_a_total[nr, :, :] = analysis_fcst[0:Nx, :]  # Update the state analysis for each realisation
        if BC:  # If using bias correction
            bias_a_total[nr, :, :] = analysis_fcst[Nx:Nx+Nb, :]  # Update the bias analysis for each realisation
            bias_b_total[nr, :, :] = bg_info['fg_traj'][Nx:Nx+Nb, :]  # Update the bias background for each
            # realisation
            if WC:
                wc_a_total[nr, :, :] = analysis_fcst[Nx + Nb: Nx + Nb + Ne, :]  # Update the model bias analysis for
                # each realisation
                wc_b_total[nr, :, :] = bg_info['fg_traj'][Nx + Nb:Nx+Nb + Ne, :]  # Update the bias background for each
                # realisation
        elif WC:
                wc_a_total[nr, :, :] = analysis_fcst[Nx: Nx + Ne, :]  # Update the model bias analysis for
                # each realisation
                wc_b_total[nr, :, :] = bg_info['fg_traj'][Nx:Nx + Ne, :]  # Update the bias background for each
                # realisation
        state_b_total[nr, :, :] = bg_info['fg_traj'][0:Nx, :]  # Update the state background for each realisation
    # print("bias_a_total =", bias_a_total)
    # print("state_total =", state_a_total[:, Nx-1, :])
    # print("shape of state_a_ttotal =", np.shape(state_a_total))
    # print("shape of bias_a_total =", np.shape(bias_a_total))
    dict = {'state_a_total': state_a_total, 'state_b_total': state_b_total, 'Ob_info': Ob_info,
            'obs_total_ar': obs_total_ar, 'Ny': Ny}
    if BC:
        dict['bias_a_total'] = bias_a_total
        dict['bias_b_total'] = bias_b_total
    if WC:
        dict['model_a_total'] = wc_a_total
        dict['model_b_total'] = wc_b_total
    # if N_c >= 2 and L_a>=Obs_sep_time[0]:  # When there are more than or equal to two cycles
    #     dict['innov0'] = innov0
    #     dict['innov1'] = innov1
    return dict


def ave_and_std(state_a_total, state_b_total, bias_a_total, BC, mbias_a_total=0, WC=0):
    """Calculate the mean and standard deviation of the state and the bias coefficient analyses over all
    # time and a given state variable for all realisations."""
    window_len = L_a*N_c+L_f-1
    state_ave = np.zeros((num_state_plot, window_len))  # Initialise array to hold average of state at given
    # variable, at all times over all realisations.
    state_std = np.zeros((num_state_plot, window_len))  # Initialise array to hold standard deviation of state at
    # given variable, at all times over all realisations.
    background_ave = np.zeros((num_state_plot, window_len))  # Initialise array to hold average of background at
    # given variable, at all times over all realisations.
    background_std = np.zeros((num_state_plot, window_len))  # Initialise array to hold standard deviation of
    # background at given variable, at all times over all realisations.
    state_error_std = np.zeros((num_state_plot, window_len))  # Initialise array to hold average standard deviation
    # of error between state and truth.
    if BC:  # If using bias correction
        bias_ave = np.zeros((Nb, window_len))  # Initialise array to hold average of bias coefficient at given variable, at
        # all times over all realisations.
        bias_std = np.zeros((Nb, window_len))
        # bias_error = np.zeros(Nb, window_len)  # Initialise array to hold the
        # difference between bias and truth for all realisations.
        bias_error_std = np.zeros((Nb, window_len))
    if WC:
        mbias_ave = np.zeros((Ne, window_len))  # Initialise array to hold average of bias coefficient at given variable, at
        # all times over all realisations.
        mbias_std = np.zeros((Ne, window_len))
        mbias_error_std = np.zeros((Ne, window_len))
    for t in range(window_len):  # Over all analysis time
        for VarIndex in range(num_state_plot):  # Over given state variable
            ob_time_plot = (VarIndex) * var_incr  # The state variable to plot
            state_ave[VarIndex, t] = np.mean(state_a_total[:, ob_time_plot, t])  # Calculate average of state at given
            # time and given state variable.
            state_std[VarIndex, t] = np.std(state_a_total[:, ob_time_plot, t])  # Calculate standard deviation of state
            # at given time and given state variable
            background_ave[VarIndex, t] = np.mean(state_b_total[:, ob_time_plot, t])  # Calculate average of background
            # at given time and given state variable.
            background_std[VarIndex, t] = np.std(state_b_total[:, ob_time_plot, t])  # Calculate std of background
            state_error = np.zeros(np.size(state_a_total[:, ob_time_plot, t]))  # Initialise array to hold the
            # difference between state and truth for all realisations.
            for i in range(np.size(state_a_total[:, ob_time_plot, t])):
                state_error[i] = np.subtract(state_a_total[i, ob_time_plot, t], true_traj[ob_time_plot, t])
            state_error_std[VarIndex, t] = np.std(state_error)
        for VarIndex in range(Nb):
            if BC:  # If using bias correction
                bias_ave[VarIndex, t] = np.mean(bias_a_total[:,VarIndex, t])  # Calculate average of bias analysis at given time.
                bias_std[VarIndex, t] = np.std(bias_a_total[:, VarIndex, t])  # Calculate standard deviation of bias analysis at given
                # time.
                # for i in range(np.size(bias_a_total[:, 0, t])):
                #     bias_error[VarIndex, i] = np.subtract(bias_a_total[i, 0, t], true_b)
                # bias_error_std[t] = np.std(bias_error)
        for VarIndex in range(Ne):
            if WC:  # If using bias correction
                mbias_ave[VarIndex, t] = np.mean(mbias_a_total[:, VarIndex, t])  # Calculate average of bias analysis at given time.
                mbias_std[VarIndex, t] = np.std(mbias_a_total[:, VarIndex, t])  # Calculate standard deviation of bias analysis at given
                # time.
                # mbias_error = np.zeros(np.size(mbias_a_total[:, ob_time_plot, t]))  # Initialise array to hold the
                # # difference between bias and truth for all realisations.
                # for i in range(np.size(mbias_a_total[:, 0, t])):
                #     mbias_error[i] = np.subtract(mbias_a_total[i, 0, t], true_b)
                # mbias_error_std[t] = np.std(mbias_error)
        # background_ave[VarIndex, window_len - 1] = np.mean(state_b_total[:, ob_time_plot, window_len])
        # # Calculate average of background at the final time time and given state variable.
        # background_std[VarIndex, window_len - 1] = np.std(state_b_total[:, ob_time_plot, window_len])
        # # Calculate standard deviation of background at the final time time and given state variable.
    dict = {'state_ave': state_ave, 'state_std': state_std, 'bkd_ave': background_ave, 'bkd_std': background_std,
            'state_error_std':state_error_std}
    if BC:
        dict['bias_ave'] = bias_ave
        dict['bias_std'] = bias_std
        dict['bias_error_std'] = bias_error_std
        # print("shape of bias ave in ave function =", np.shape(bias_ave))
    if WC:
        dict['mbias_ave'] = mbias_ave
        dict['mbias_std'] = mbias_std
        dict['mbias_error_std'] = mbias_error_std
    return dict
