"""Use main_many_realisations_spread to calculate the statistics for b^a for different parameters and plot against each
other."""
import main_many_realisations_spread as many_real
import parameters as p
import numpy as np
import matplotlib.pyplot as plt
import Lorenz96
import seaborn as sns
import math
import pickle
from matplotlib.colors import TwoSlopeNorm

def b_stats_error_var(ob, obb, o1, o2, beta_b_obs, L_b, B=1):
    """Use main_many_realisations_spread to calculate b_stats for given error variances assuming there are two
    observation types. ob is the state background error variance, obb is the bias coefficient background error variance,
    o1 is the biased observation error variance and o2 is the unbiased observation error variance."""
    if p.N_ob_types == 2:
        sigma_o = [o1, o2]
    if p.N_ob_types == 1:
        sigma_o = [o1]
    BC = 1  # 1: do bias correction
    N_realisations = 5  # Number of realisations
    # Calculate states over given number of realisations
    realisation_info = many_real.all_real(N_realisations, BC, ob, sigma_o, beta_b_obs, L_b, B)
    state_a = realisation_info['state_a_total']
    state_b = realisation_info['state_b_total']
    bias_a = realisation_info['bias_a_total']
    bias_b = realisation_info['bias_b_total']
    ### Calculate bias coefficient average from realisations to give mean plus and minus standard deviation. ###
    ave = many_real.ave_and_std(state_a, state_b, bias_a, BC)  # Calculate averages and stds for the state and the bias
    # coefficient analysis.
    state_ave = ave['state_ave'][:, 0:p.L_a*p.N_c]
    # print("shape of state_ave =", np.shape(state_ave))
    state_std = ave['state_std'][:, 0:p.L_a*p.N_c]
    bkd_ave = ave['bkd_ave'][:, 0:p.L_a*p.N_c]  # State background average
    bkd_std = ave['bkd_std'][:, 0:p.L_a*p.N_c]  # State background standard deviation
    # print("shape of bias_ave =", np.shape(ave['bias_ave']))
    bias_ave = ave['bias_ave'][0:p.L_a*p.N_c]  # bias coefficient analysis average
    bias_std = ave['bias_std'][0:p.L_a*p.N_c] # bias coefficient analysis standard deviation
    # print("shape of bias_std =", np.shape(bias_std))
    bias_upper = np.add(bias_ave, bias_std)  # Add the mean and the std of the bias to calculate upper bound.
    bias_lower = np.subtract(bias_ave, bias_std)  # Subtract the mean and the std of the bias calculate the lower
    # bound.
    bias_error_std = ave['bias_error_std'][0:p.L_a*p.N_c]
    # print("shape of bias_error_std =", np.shape(bias_error_std))
    bias_bkd_ave = many_real.ave_std_bias_bkd(bias_b)[0]
    bias_bkd_std = many_real.ave_std_bias_bkd(bias_b)[1]
    return bias_ave, bias_upper, bias_lower, state_ave, state_std, bias_error_std, bias_std, bkd_ave, bkd_std, \
           bias_bkd_ave, bias_bkd_std

def true():
    """Calculate the true value for a given number of cycles over the whole spatial domain. """
    true_traj = Lorenz96.Lorenz96_nonLinear_propogation(p.Ics(), p.L_a*p.N_c-1, p.F_A)
    print("shape of true =", np.shape(true_traj))
    ave_over_time = np.zeros(p.Nx)
    for i in range(p.Nx):
        ave_over_time[i] = np.average(true_traj[i, :])
    ave_over_space = np.average(ave_over_time)
    print("ave_over space =", ave_over_space)
    return true_traj


def plot_diff_error_var():
    """Plot b^a stats when error variances are varied."""
    beta_b_obs = [1, 0]  # Add a model bias of one to all variables (for when every variable is observed by both obs.
    L_b = 0.2
    # Case 1: all error variances = 1
    ob1 = 2
    obb1 = 2
    o11 = 2
    o21 = 2
    ave_1, upper_1, lower_1, state_ave1, state_std1, bias_error_std1 = b_stats_error_var(ob1, obb1, o11, o21,
                                                                                         beta_b_obs, L_b)
    ba_std_1 = np.subtract(ave_1, lower_1)
    # state_error1 = np.subtract(state_ave1, true())  # Difference between state and true
    # ave_error1 = np.mean(state_error1) # Average state analysis error
    # ave_std1 = np.mean(state_std1)  # Average state analysis std
    # Case 2: observation error variances = 0.2
    ob2 = 2
    obb2 = 2
    o12 = 0.2
    o22 = 0.2
    ave_2, upper_2, lower_2, state_ave2, state_std2, bias_error_std2 = b_stats_error_var(ob2, obb2, o12, o22,
                                                                                         beta_b_obs, L_b)
    ba_std_2 = np.subtract(ave_2, lower_2)
    # state_error2 = np.subtract(state_ave2, true())  # Difference between state and true
    # ave_error2 = np.mean(state_error2)
    # ave_std2 = np.mean(state_std2)
    # Case 3: biased obs error var = 0.2, state background error var = 2
    ob3 = 2
    obb3 = 2
    o13 = 0.2
    o23 = 2
    ave_3, upper_3, lower_3, state_ave3, state_std3, bias_error_std3 = b_stats_error_var(ob3, obb3, o13, o23,
                                                                                         beta_b_obs, L_b)
    ba_std_3 = np.subtract(ave_3, lower_3)
    # state_error3 = np.subtract(state_ave3, true())  # Difference between state and true
    # ave_error3 = np.mean(state_error3)
    # ave_std3 = np.mean(state_std3)
    # Case 4: unbiased obs error var = 0.2, state background error var = 2
    ob4 = 2
    obb4 = 2
    o14 = 2
    o24 = 0.2
    ave_4, upper_4, lower_4, state_ave4, state_std4, bias_error_std4 = b_stats_error_var(ob4, obb4, o14, o24,
                                                                                         beta_b_obs, L_b)
    ba_std_4 = np.subtract(ave_4, lower_4)
    # state_error4 = np.subtract(state_ave4, true())  # Difference between state and true
    # ave_error4 = np.mean(state_error4)
    # ave_std4 = np.mean(state_std4)
    print("bias error ave-1 =", 2 - ave_1, ", error std-1 =", bias_error_std1, "var1 =", bias_error_std1**2)
    # print("state ave-1 =", ave_error1, "state std-1 =", ave_std1)
    print("bias error ave-2 =", 2 - ave_2, ", std-2 =", bias_error_std2, "var2 =", bias_error_std2**2)
    # print("state ave-2 =", ave_error2, "state std-2 =", ave_std2)
    print("bias error ave-3 =", 2- ave_3, ", std-3 =", bias_error_std3, "var3 =", bias_error_std3**2)
    # print("state ave-3 =", ave_error3, "state std-3 =", ave_std3)
    print("bias error ave-4 =", 2 - ave_4, ", std-4 =", bias_error_std4, "var4 =", bias_error_std4**2)
    # print("state ave-4 =", ave_error4, "state std-4 =", ave_std4)
    # Plot b^a stats
    xax = [1, 2, 3, 4]  # Define x axis to be each case.
    plt.figure(1)
    plt.rcParams['font.size'] = '12'
    plt.plot(xax[0], ave_1, 'o', c='m', label='All error var = 1')
    plt.plot(xax[0], upper_1, '_', c='m')
    plt.plot(xax[0], lower_1, '_', c='m')
    plt.fill_between(xax[0], lower_1, upper_1, color='m')
    plt.plot(xax[1], ave_2, 'o', c='g', label='$\sigma_{o(1)} = \sigma_{o(2)} = 0.2$')
    plt.plot(xax[1], upper_2, '_', c='g')
    plt.plot(xax[1], lower_2, '_', c='g')
    plt.fill_between(xax[1], lower_2, upper_2, color='g')
    plt.plot(xax[2], ave_3, 'o', c='b', label='$\sigma_{o(1)} = 0.2$')
    plt.plot(xax[2], upper_3, '_', c='b')
    plt.plot(xax[2], lower_3, '_', c='b')
    plt.fill_between(xax[2], lower_3, upper_3, color='b')
    plt.plot(xax[3], ave_4, 'o', c='r', label='$\sigma_{o(2)} = 0.2$')
    plt.plot(xax[3], upper_4, '_', c='r')
    plt.plot(xax[3], lower_4, '_', c='r')
    plt.fill_between(xax[3], lower_4, upper_4, color='r')
    labels = ["All error var = 1", "$\sigma_{o(1)}^2 = \sigma_{o(2)}^2 = 0.2$", "$\sigma_{o(1)}^2 = 0.2$",
                                                                        "$\sigma_{o(2)}^2 = 0.2$"]
    plt.xticks(xax, labels, rotation='horizontal', fontsize=16)
    # plt.yticks(size=16)
    plt.ylabel("Bias coefficient analysis statistics", fontsize=16)
    plt.tick_params(labelright=True, right=True)
    plt.grid(axis='y')
    plt.show()
    # plt.figure(2)
    # plt.plot(state_error1, c='r')
    # plt.plot(state_error2, c='m')
    # plt.plot(state_error3, c='b')
    # plt.plot(state_error4, c='g')
    # plt.show()


def plot_loc_model_bias():
    """Plot a figure that plots the bias coefficient analysis statistics when model bias is only observed by biased
    observations, only observed by anchor observations and observed by both."""
    ob = 1  # state background error variance
    obb = 1  # bias coefficient background error variance
    o1 = 1  # biased observation error variance
    o2 = 1  # anchor observation error variance
    L_b1 = 0.2  # Length scale of B matrix - determines background error covariances.
    L_b2 = 2  # Length scale of B matrix - determines background error covariances.
    ### Model bias only in state observed by biased observation ###
    beta_b1 = [1, 0]  # background bias added to specific states observed by observations.
    ave_11, upper_11, lower_11, state_ave11, state_std11, bias_error_std11, bias_std, bkd_ave, bkd_std, \
           bias_bkd_ave, bias_bkd_std = b_stats_error_var(ob, obb, o1, o2, beta_b1, L_b1)  # Small background covariances
    ave_12, upper_12, lower_12, state_ave12, state_std12, bias_error_std12 = b_stats_error_var(ob, obb, o1, o2, beta_b1, L_b2)  # Large background covariances
    ### Model bias only in state observed by anchor observation ###
    beta_b2 = [0, 1]
    ave_21, upper_21, lower_21, state_ave21, state_std21, bias_error_std21 = b_stats_error_var(ob, obb, o1, o2, beta_b2, L_b1)  # Small background covariances
    ave_22, upper_22, lower_22, state_ave22, state_std22, bias_error_std22 = b_stats_error_var(ob, obb, o1, o2, beta_b2, L_b2)  # Large background covariances
    ### Model bias in both states ###
    beta_b3 = [1, 1]
    ave_31, upper_31, lower_31, state_ave31, state_std31, bias_error_std31 = b_stats_error_var(ob, obb, o1, o2, beta_b3, L_b1)
    ave_32, upper_32, lower_32, state_ave32, state_std32, bias_error_std32 = b_stats_error_var(ob, obb, o1, o2, beta_b3, L_b2)
    # print("b^a ave when model bias in biased obs =", ave_1)
    # print("b^a ave when model bias in anchor obs =", ave_2)
    # print("b^a ave when model bias in both =", ave_3)
    print("bias ave11 =", ave_11, ", bias error var =", bias_error_std11**2)
    print("bias ave12 =", ave_12, ", bias error var =", bias_error_std12**2)
    print("bias ave21 =", ave_21, ", bias error var =", bias_error_std21**2)
    print("bias ave22 =", ave_22, ", bias error var =", bias_error_std22**2)
    print("bias ave31 =", ave_31, ", bias error var =", bias_error_std31**2)
    print("bias ave32 =", ave_32, ", bias error var =", bias_error_std32**2)
    # Plot b^a stats
    xax = np.arange(0, 100)  # Define x axis to be each case.
    plt.figure(1)
    plt.rcParams['font.size'] = '12'
    plt.plot(xax[32], ave_11, 'o', c='m', label='Model bias only observed by biased observation')
    plt.plot(xax[32], upper_11, '_', c='m')
    plt.plot(xax[32], lower_11, '_', c='m')
    plt.fill_between(xax[32], lower_11, upper_11, color='m')
    plt.plot(xax[34], ave_12, 'x', c='m', label='Model bias only observed by biased observation')
    plt.plot(xax[34], upper_12, '_', c='m')
    plt.plot(xax[34], lower_12, '_', c='m')
    plt.fill_between(xax[34], lower_12, upper_12, color='m')
    plt.plot(xax[65], ave_21, 'o', c='g', label='Model bias only observed by anchor observation')
    plt.plot(xax[65], upper_21, '_', c='g')
    plt.plot(xax[65], lower_21, '_', c='g')
    plt.fill_between(xax[65], lower_21, upper_21, color='g')
    plt.plot(xax[67], ave_22, 'x', c='g', label='Model bias only observed by anchor observation')
    plt.plot(xax[67], upper_22, '_', c='g')
    plt.plot(xax[67], lower_22, '_', c='g')
    plt.fill_between(xax[67], lower_22, upper_22, color='g')
    plt.plot(xax[97], ave_31, 'o', c='b', label='Model bias observed by both biased and unbiased observations')
    plt.plot(xax[97], upper_31, '_', c='b')
    plt.plot(xax[97], lower_31, '_', c='b')
    plt.fill_between(xax[97], lower_31, upper_31, color='b')
    plt.plot(xax[99], ave_32, 'x', c='b', label='Model bias observed by both biased and unbiased observations')
    plt.plot(xax[99], upper_32, '_', c='b')
    plt.plot(xax[99], lower_32, '_', c='b')
    plt.fill_between(xax[99], lower_32, upper_32, color='b')
    plt.xticks([33, 66, 98], ["Model bias only observed \n by biased observations", "Model bias only observed \n by anchor observations",
              "Model bias observed by \n both biased and unbiased \n observations"], fontsize=12)
    plt.ylabel("Bias coefficient analysis statistics", fontsize=12)
    plt.grid(axis = 'y')
    # plt.figtext(0.5, 0.5, 'matplotlib', horizontalalignment='center', verticalalignment='center')
    plt.figtext(0.2, 0.8, 'Circles: $L_b = 0.2$ \n Crosses: $L_b = 1$', bbox=dict(facecolor='white', edgecolor='black'))
    # plt.tick_params(labelright=True, right=True)
    plt.show()



def plot_bias_ratio_vary_Lb():
    """Plot the ratio of the difference between the true and analysis bias coefficient, whilst varying the B length
    scale.Plot for three cases: model bias only in states observed by biased observations; only in states observed by
    anchor observations; and model bias in both states. MAKE SURE TO CHOOSE OBSERVATIONS THAT ARE SHIFTED IN PARAMETERS,
    set L_a = L_f = N_c = 1 and set initial = 1 in main_many_realisations_spread line 106 so that B is not calculated
    from initial ensemble."""
    ob = 1  # state background error variance
    obb = 1  # bias coefficient background error variance
    o1 = 1  # biased observation error variance
    o2 = 1  # anchor observation error variance
    num_sam = 2  # Number of length scales to use
    L = np.linspace(0.2, 2, num=num_sam)
    initB = pickle.load(open("B_Fa_8.8_Nx_40_BC_1_Nobtypes_2_Obs_sep_dist_2_2_La_10_Lf_1_dt_0.0125_o1_1_o2_1_ob_1_obb_0.5.dat", "rb"))
    # Load initial cycle for state from previously saved data file, "rb" stands for read binary file.
    ### Model bias only in state observed by biased observation ###
    ratio_1 = np.zeros(num_sam)  # Create a zero array to hold all ratio values for the first case
    beta_b1 = [0.3, 0]  # background bias added to specific states observed by biased observations.
    for i in range(num_sam):  # Loop over each length scale.
        inflatedB = np.multiply(initB, L[i]*np.ones((p.Nx + p.Nb, p.Nx+ p.Nb)))
        b_stats = b_stats_error_var(ob, obb, o1, o2, beta_b1, 1, B=inflatedB)
        ave_1 = b_stats[0]
        bias_std_1 = b_stats[5]
        diff = np.subtract(ave_1, p.true_b)  # Subtract the true bias coefficient from the average bias coefficient
        # analysis
        ratio_1[i] = np.abs(np.divide(diff, o1))  # Find the ratio between the difference and the bias std
    ### Model bias only in state observed by anchor observation ###
    ratio_2 = np.zeros(num_sam)  # Create a zero array to hold all ratio values for the second case.
    beta_b2 = [0, 0.3]  # background bias added to specific states observed an anchor observations
    # for i in range(num_sam):  # Loop over each length scale.
    #     b_stats = b_stats_error_var(ob, obb, o1, o2, beta_b2, L_b[i])
    #     ave_2 = b_stats[0]
    #     bias_std_2 = b_stats[5]
    #     diff = np.subtract(ave_2, p.true_b)  # Subtract the true bias coefficient from the average bias coefficient
    #     # analysis
    #     ratio_2[i] = np.abs(np.divide(diff, o1))  # Find the ratio between the difference and the bias std
    # ### Model bias in both states ###
    # ratio_3 = np.zeros(num_sam)  # Create a zero array to hold all ratio values for the third case.
    # beta_b3 = [0.3, 0.3]  # background bias added to specific states observed by either type of observation.
    # for i in range(num_sam):  # Loop over each length scale.
    #     b_stats = b_stats_error_var(ob, obb, o1, o2, beta_b3, L_b[i])
    #     ave_3 = b_stats[0]
    #     bias_std_3 = b_stats[5]
    #     diff = np.subtract(ave_3, p.true_b)  # Subtract the true bias coefficient from the average bias coefficient
    #     # analysis
    #     ratio_3[i] = np.abs(np.divide(diff, o1))  # Find the ratio between the difference and the bias std
    # # Plot all three cases on one plot.
    # plt.figure(figsize=(15, 7))
    # plt.rcParams['font.size'] = '16'
    # plt.plot(L_b, ratio_1, label='Model bias only in states observed by biased observations', linewidth=2)
    # plt.plot(L_b, ratio_2, label='Model bias only in states observed by anchor observations', linewidth=2)
    # plt.plot(L_b, ratio_3, label='Model bias in states observed by both biased and anchor observations', linewidth=2)
    # plt.axhline(y=0.1, color='black', linestyle='--')
    # plt.xlabel('$L_b$')
    # plt.ylabel('$|b^a - b^t|/ \sigma_{ab}$')
    # plt.legend()
    # plt.show()


def plot_bias_ratio_vary_o1o2():
    """Plot the ratio of the difference between the true and analysis bias coefficient, whilst varying the observation
     error variances. MAKE SURE TO CHOOSE OBSERVATIONS THAT ARE NOT SHIFTED IN PARAMETERS."""
    ob = 1  # state background error variance
    obb = 1  # bias coefficient background error variance
    L_b = 0.2  # background error covariance length scale
    beta_b = [0.3, 0]  # background bias added to all states observed by observations.
    num_sam = 8  # Number of length scales to use
    o1 = np.linspace(0.2, 2, num=num_sam)  # biased observation error variance
    o2 = np.linspace(0.2, 2, num=num_sam)  # anchor observation error variance
    ### Calculate bias coefficient ratio for each o1 and o2. ###
    ratio = np.zeros((num_sam, num_sam))  # Create a zero array to hold all ratio values
    print("for loop starting")
    for i in range(num_sam):  # Loop over each o1.
        for j in range(num_sam):  # Loop over each o2.
            b_stats = b_stats_error_var(ob, obb, o1[i], o2[j], beta_b, L_b)
            ave_1 = b_stats[0]
            bias_std_1 = b_stats[5]
            diff = np.subtract(ave_1, p.true_b)  # Subtract the true bias coefficient from the average bias coefficient
            # analysis
            # print("diff =", diff)
            print("bias_std_1 =", bias_std_1)
            print("shape of bias average =", np.shape(ave_1))
            print("shape of diff =", np.shape(diff))
            ratio[i, j] = np.abs(np.divide(diff, bias_std_1))  # Find the ratio between the difference and the bias
            # std
    o1_label = np.round(o1, decimals=1)
    o2_label = np.round(o2, decimals=1)
    plt.figure()
    plt.rcParams['font.size'] = '12'
    # plt.contour(ratio, levels=([0.1]), colors='yellow')
    ax = sns.heatmap(ratio, xticklabels=o1_label, yticklabels=o2_label, cbar_kws={'label': '$|b^a - b^t|/ \sigma_{ab}$'})
    plt.ylabel('$\sigma_{o(1)}$')
    plt.xlabel('$\sigma_{o(2)}$')
    ax.invert_yaxis()  # Plot y-axis as increasing
    plt.show()


def plot_bias_ratio_vary_o2():
    """Plot the ratio of the difference between the true and analysis bias coefficient, whilst varying the observation
     error variances. MAKE SURE TO CHOOSE OBSERVATIONS THAT ARE NOT SHIFTED IN PARAMETERS."""
    ob = 1  # state background error variance
    obb = 1  # bias coefficient background error variance  # NOT USED, CHANGE IN PARAMETERS
    o1 = 1  # biased observation error variance
    L_b = 0.2  # background error covariance length scale
    beta_b = [0.3, 0]  # background bias added to all states observed by observations.
    num_sam = 8  # Number of length scales to use
    o2 = np.linspace(0.2, 2, num=num_sam)  # anchor observation error variance
    ### Calculate bias coefficient ratio for each o1 and o2. ###
    ratio = np.zeros(num_sam)  # Create a zero array to hold all ratio values
    for j in range(num_sam):  # Loop over each o2.
        b_stats = b_stats_error_var(ob, obb, o1, o2[j], beta_b, L_b)
        ave_1 = b_stats[0]
        bias_std_1 = b_stats[5]
        diff = np.subtract(ave_1, p.true_b)  # Subtract the true bias coefficient from the average bias coefficient
        # analysis
        # print("diff =", diff)
        # print("bias_std_1 =", bias_std_1)
        print("shape of bias average =", np.shape(ave_1))
        print("shape of diff =", np.shape(diff))
        print("shape of bias_std in for loop =", np.shape(bias_std_1))
        print("np.divide(diff, bias_std_1) =", np.divide(diff, bias_std_1))
        ratio[j] = np.abs(np.divide(diff[0], bias_std_1[0]))  # Find the ratio between the difference and the bias
        # std (only want first value in assimilation window)
    o2_label = np.round(o2, decimals=1)
    plt.figure()
    plt.rcParams['font.size'] = '12'
    # plt.contour(ratio, levels=([0.1]), colors='yellow')
    plt.plot(o2_label, ratio, label='Ratio of bias in bias coefficient')
    plt.xlabel('$\sigma_{o(2)}$')
    plt.ylabel('$|b^a - b^t|/\sigma_{ab}$')
    plt.show()


def cycling_ratio():
    """Plot the ratio of the bias in two states plus bias coefficient over several cycles."""
    ob = 0.5  # state background error variance
    obb = 0.5  # bias coefficient background error variance
    L_b = 0.2  # background error covariance length scale
    beta_b = [0, 0]  # background bias added to all states observed by observations.
    o1 = 0.5  # biased observation error variance
    o2 = 0.5  # anchor observation error variance
    true_traj = Lorenz96.Lorenz96_nonLinear_propogation(p.Ics(), p.L_a * p.N_c + p.L_f - 1, p.F)
    var_incr = 20  # increment between variables to plot
    N = len(true_traj[:, 0])  # Number of spatial variables
    num_state_plot = math.ceil(N / var_incr) + 1  # number of variables to plot, plus bias coefficient
    xx = range(p.L_a * p.N_c)  # Range on x axis - number of time steps
    analysis = b_stats_error_var(ob, obb, o1, o2, beta_b, L_b)  # state, bias coefficient analysis mean/std
    x_mean = analysis[3]
    # print("shape of x mean =", np.shape(x_mean))
    b_mean = analysis[0]
    # print("shape of b_mean =", np.shape(b_mean))
    # print("b_mean =", b_mean)
    x_std = analysis[4]
    b_std = analysis[6]
    xb = analysis[7]  # state background
    plt.figure(1)
    plt.subplots(num_state_plot, 1)  # Plot the number of subplots equal to the number of state variables to plot.
    xc_label = np.arange(0, p.N_c, 5)
    xc = np.arange(0, p.N_c)
    for VarIndex in range(num_state_plot - 1):  # Plot the ratio for each state variable being plotted
        plt.subplot(num_state_plot, 1, VarIndex + 1)
        ob_time_plot = (VarIndex) * var_incr  # The state variable to plot
        if p.L_a >= p.Obs_sep_time[0]:  # If doing 3DVar over one time step or 4DVar over multiple time steps, subtract
            # whole of truth state
            diff = np.subtract(x_mean[VarIndex, :], true_traj[ob_time_plot, 0:p.L_a * p.N_c])  # Subtract the true state
            # from the averagestate analysis
            state_ratio = np.abs(np.divide(diff, x_std[VarIndex, :]))
        else:  # If using 3DVar but over multiple time steps for each window, only use beginning of cycle.
            diff = np.zeros(p.N_c)  # Initialise diff to be the number of cycles
            state_ratio = np.zeros(p.N_c)
            for c in range(0, p.N_c):
                diff[c] = np.subtract(x_mean[VarIndex, p.L_a*c], true_traj[ob_time_plot, p.L_a * c])  # Subtract the true state
                print("x_mean[", VarIndex, p.L_a*c, "] = ", x_mean[VarIndex, p.L_a*c])
                print("true_traj[", ob_time_plot, p.L_a*c, "] = ", true_traj[ob_time_plot, p.L_a*c])
                # from the averagestate analysis
                state_ratio[c] = np.abs(np.divide(diff[c], x_std[VarIndex, p.L_a*c]))
        plt.plot(xc, state_ratio, 'o-', c='r', label='Ratio of Bias in State')  # Plot analysis ratio
        plt.axhline(y=0.1, color='black', linestyle='--')
        plt.ylabel("x" + str(ob_time_plot), fontsize=20)
        plt.xticks(xc_label, size=14)
        plt.yticks(size=14)
    plt.tight_layout()
    plt.legend(loc="lower right", fontsize=16)
    plt.subplot(num_state_plot, 1, num_state_plot)  # Plot the bias coefficient ratio
    if p.L_a >= p.Obs_sep_time[0]:  # If doing 3DVar over one time step or 4DVar over multiple time steps, subtract
        # whole of truth state
        diff_b = np.subtract(b_mean, p.true_b)  # Subtract the true bias coefficient from the average bias coefficient
        # analysis
        bias_ratio = np.abs(np.divide(diff_b, obb))  # Find the ratio between the difference and the bias std
    else:  # Else if doing 3DVar over multiple time steps just take first value in cycle.
        diff_b = np.zeros(p.N_c)
        bias_ratio = np.zeros(p.N_c)
        for c in range(p.N_c):
            diff_b[c] = np.subtract(b_mean[p.L_a*c], p.true_b)  # Subtract the true bias coefficient from the average
            # bias coefficient analysis
            bias_ratio[c] = np.abs(np.divide(diff_b[c], obb))  # Find the ratio between the difference and the bias std
    print("bias_diff =", diff_b)
    # print("bias ratio =", bias_ratio)
    plt.plot(xc, bias_ratio, 'o-', c='m', label='Ratio of Bias in Bias Coefficient')  # Plot bias coefficient ratio
    plt.axhline(y=0.1, color='black', linestyle='--')
    plt.ylabel("$b$", fontsize=20)
    plt.xticks(xc_label, size=14)
    plt.yticks(size=14)
    plt.xlabel("Number of cycles", fontsize=20)
    plt.legend(loc="lower right", fontsize=16)
    plt.tight_layout()
    plt.show()
    plt.figure(3)
    plt.subplots(num_state_plot - 1, 1)  # Plot the number of subplots equal to the number of state variables to plot.
    for VarIndex in range(num_state_plot - 1):  # Plot the ratio for each state variable being plotted
        plt.subplot(num_state_plot, 1, VarIndex + 1)
        ob_time_plot = (VarIndex) * var_incr  # The state variable to plot
        plt.plot(xc, xb[VarIndex, :], 'o-', c='blue', label='Mean state background')  # Plot mean state background
        plt.plot(xc, true_traj[ob_time_plot, 0:p.L_a * p.N_c], c='black', label = 'True trajectory')
        plt.ylabel("$x^b$" + str(ob_time_plot), fontsize=20)
        plt.xticks(xc_label, size=14)
        plt.yticks(size=14)
    plt.tight_layout()
    plt.legend(loc="lower right", fontsize=16)
    plt.show()


def model_error():
    """Calculate the model error between the true model and a model with different forcing. """
    truetraj = true()  # Calculate true trajectory.
    # forcedtraj = Lorenz96.Lorenz96_nonLinear_propogation(p.Ics(), p.L_a*p.N_c-1, p.F_A)  # Calculate the model trajectory
    # when the forcing is different.
    forcedtraj = Lorenz96.Lorenz96_nonLinear_propogation(p.F_Ics(), p.L_a * p.N_c - 1, p.F_A)
    print("shape of truetraj =", np.shape(truetraj))
    print("shape of forced traj =", np.shape(forcedtraj))
    diff0 = np.subtract(truetraj[0, :], forcedtraj[0, :])  # Difference between true and forced trajectory means for x0.
    diff20 = np.subtract(truetraj[20, :], forcedtraj[20, :])  # Difference between true and forced trajectory means for x20.
    true_mean = np.zeros(p.Nx)  # Initialise an array to hold mean values for every state variable.
    forced_mean = np.zeros(p.Nx)  # Initialise an array to hold mean values for every state variable.
    for i in range(p.Nx):
        true_mean[i] = np.mean(truetraj[i, :])  # Mean value of true trajectory for each state.
        forced_mean[i] = np.mean(forcedtraj[i, :])  # Mean value of forced trajectory for each state.
    mean_diff = abs(np.subtract(true_mean, forced_mean))  # Find the difference between the true and forced means.
    print("shape of diff =", np.shape(mean_diff))
    plt.subplots(2, 1)  # Plot the difference between the truth and the forced model over 100 cycles for two state vars.
    plt.subplot(2, 1, 1)
    plt.plot(diff0, color='red', label='Difference between x0 and true')
    plt.legend(loc='upper right')
    plt.xlabel('Number of cycles')
    plt.subplot(2, 1, 2)
    plt.plot(diff20, color='red', label='Difference between x20 and true')
    plt.tight_layout()
    plt.legend(loc='upper right')
    plt.xlabel('Number of cycles')
    plt.show()
    plt.figure(2)  # Plot mean difference between true and forced trajectories from 1000 cycles at each spatial variable.
    plt.plot(mean_diff, label='Difference between mean true trajectory and mean forced trajectory over 1000 cycles')
    plt.xlabel('Spatial variables: $x_0$ to $x_{39}$')
    plt.legend()
    plt.show()
    # RMSE of all states for each cycle.
    RMSE = np.zeros(p.L_a*p.N_c)  # Initialise RMSE array to be zeros for each cycle.
    for j in range(p.L_a*p.N_c):  # Calculate RMSE for each cycle
        sum = 0
        for i in range(p.Nx):   # Sum over every state variable to calculate RMSE
            sum += (truetraj[i, j] - forcedtraj[i, j])**2
        RMSE[j] = np.sqrt(sum/p.Nx)  # Calculate RMSE
    plt.figure(3)  # Plot the RMSE over all spatial variables for 100 cycles.
    plt.plot(RMSE)
    plt.xlabel('Number of cycles')
    plt.ylabel('RMSE')
    plt.show()
    diff = abs(np.subtract(truetraj[:, 1], forcedtraj[:, 1]))  # Find the difference between the true and forced models
    # after the first cycle.
    plt.figure(4)
    plt.plot(diff, label='Difference between mean and forced trajectories after 1 cycle')
    plt.xlabel('Spatial variables: $x_0$ to $x_{39}$')
    plt.legend()
    plt.show()
    true_space_mean = np.mean(true_mean)
    forced_space_mean = np.mean(forced_mean)
    print("true mean =", true_space_mean)
    print("forced mean =", forced_space_mean)
    print("difference between true and forced mean =", np.subtract(forced_space_mean, true_space_mean))
    plt.figure(5)
    plt.plot(true_mean, 'o-', color='red', label='Mean of true trajectory across 1000 cycles')
    plt.axhline(true_space_mean, linestyle='--', color='red')
    plt.plot(forced_mean, 'o-', color='blue', label='Mean of forced trajectory across 1000 cycles')
    plt.axhline(forced_space_mean, linestyle='--', color='blue')
    plt.xlabel('Spatial variables: $x_0$ to $x_{39}$')
    plt.legend()
    plt.show()


def an_bkd_error():
    """Calculate the difference between the mean analysis and the truth and the difference between the mean background
    and the truth for states x0 and x20."""
    beta_b_obs = [0, 0]  # Model bias through forcing term only
    L_b = 0.2
    ob = 0.5
    obb = 0.5
    o1 = 0.5
    o2 = 0.5
    mean_vals = b_stats_error_var(ob, obb, o1, o2, beta_b_obs, L_b)
    mean_ana = mean_vals[3]  # mean state analysis for x0 and x20
    mean_bkd = mean_vals[7]  # mean state background for x0 and x20
    bias_bkd = mean_vals[9]  # mean bias coefficient background
    bias_ana = mean_vals[0]  # mean bias coefficient analysis
    print("shape of mean ana =", np.shape(mean_ana))
    print("shape of mean bkd =", np.shape(mean_bkd))
    print("shape of bias bkd =", np.shape(bias_bkd))
    print("shape of bias ana =", np.shape(bias_ana))
    print("shape of true =", np.shape(true()))
    # Find the difference between the state analysis and the truth across all cycles
    ana_diff_0 = abs(np.subtract(mean_ana[0, :], true()[0, :]))  # for x0
    ana_diff_20 = abs(np.subtract(mean_ana[1, :], true()[20, :]))  # for x20
    # Find the difference between the state background and the truth across all cycles
    bkd_diff_0 = abs(np.subtract(mean_bkd[0, 0:np.size(mean_bkd[0, :]) -1], true()[0, :]))  # for x0
    bkd_diff_20 = abs(np.subtract(mean_bkd[1, 0:np.size(mean_bkd[0, :]) -1], true()[20, :]))  # for x20
    # Find the difference between the bias analysis/background and the true bias across all cycles
    true_b = p.true_b*np.ones(p.L_a*p.N_c)  # The true bias coefficient over all cycles.
    ana_diff_b = abs((np.subtract(bias_ana, true_b)))  # Difference between bias analysis and truth
    bkd_diff_b = abs(np.subtract(bias_bkd[0:np.size(bias_bkd) - 1], true_b))  # Difference between bias background and truth
    plt.subplots(3, 1)
    plt.subplot(3, 1, 1)
    plt.plot(ana_diff_0, 'o-', color='red', label='Absolute value of $x_{0}$ analysis difference')
    plt.plot(bkd_diff_0, 'o-', color='blue', label='Absolute value of $x_{0}$ background difference')
    plt.ylabel('$x_{0}$', fontsize=20)
    plt.yticks([0, 0.02, 0.04, 0.06, 0.08, 0.1, 0.12, 0.14, 0.16], size=14)
    plt.xticks(size=14)
    plt.legend(loc='lower right', fontsize=16)
    plt.subplot(3, 1, 2)
    plt.plot(ana_diff_20, 'o-', color='red', label='Absolute value of $x_{20}$ analysis difference')
    plt.plot(bkd_diff_20, 'o-', color='blue', label='Absolute value of $x_{20}$ background difference')
    plt.ylabel('$x_{20}$', fontsize=20)
    plt.legend(loc='lower right', fontsize=16)
    plt.yticks([0, 0.02, 0.04, 0.06, 0.08, 0.1, 0.12, 0.14, 0.16], size=14)
    plt.xticks(size=14)
    plt.subplot(3, 1, 3)
    plt.plot(ana_diff_b, 'o-', color='red', label='Absolute value of $b$ analysis difference')
    plt.plot(bkd_diff_b, 'o-', color='blue', label='Absolute value of $b$ background difference')
    # plt.plot(bias_bkd, 'o-', color='green', label='$b$ background')
    # plt.plot(bias_ana, 'o-', color='black', label='$b$ analysis')
    plt.ylabel('$b$', fontsize=20)
    plt.xlabel('Number of cycles', fontsize=20)
    plt.xticks(size=14)
    plt.yticks([0, 0.02, 0.04, 0.06, 0.08], size=14)
    plt.legend(loc='lower right', fontsize=16)
    plt.show()


def cycling_norm():
    """Calculate the state and bias coefficient analyses over several cycles. Plot the norm of the whole state instead
    of two variables. ####To calculate this, we need num_state_plot in main_many_realisations to be 40, usually 2!###"""
    ob = 1  # state background error variance
    obb = 1  # bias coefficient background error variance
    L_b = 0.5 #2  # background error covariance length scale
    beta_b = [0, 0]  # background bias added to all states observed by observations.
    o1 = 0.5  # biased observation error variance
    o2 = 0.5  # anchor observation error variance
    true_traj = Lorenz96.Lorenz96_nonLinear_propogation(p.Ics(), p.L_a * p.N_c + p.L_f - 1, p.F)
    analysis = b_stats_error_var(ob, obb, o1, o2, beta_b, L_b)  # state, bias coefficient analysis mean/std
    x_mean = analysis[3]
    # print("shape of x mean =", np.shape(x_mean))
    b_mean = analysis[0]
    # print("shape of b_mean =", np.shape(b_mean))
    # print("b_mean =", b_mean)
    x_std = analysis[4]
    if p.L_a >= p.Obs_sep_time[0]:  # If doing 3DVar over one time step or 4DVar over multiple time steps, subtract
            # whole of truth state
        diff = np.zeros((p.Nx, p.L_a * p.N_c))
        for VarIndex in range(p.Nx):  # Plot the ratio for each state variable being plotted
            diff[VarIndex, :] = np.subtract(x_mean[VarIndex, :], true_traj[VarIndex, 0:p.L_a * p.N_c])  # Subtract the
            # true state from the average state analysis
        norm = np.zeros(p.L_a * p.N_c)
        mean_std = np.zeros(p.L_a * p.N_c)
        state_ratio = np.zeros(p.L_a * p.N_c)
        for t in range(p.L_a * p.N_c):
            norm[t] = np.linalg.norm(diff[:, t])  # Calculate norm of differences for each state.
            mean_std[t] = np.mean(x_std[:, t])  # Calculate the mean standard deviation of all states
            state_ratio[t] = np.divide(norm[t], p.Nx*mean_std[t])  # Calculate the ratio between the bias and the variance at
            # each time step.
    else:  # If using 3DVar but over multiple time steps for each window, only use beginning of cycle.
        diff = np.zeros((p.Nx, p.N_c))  # Initialise diff to be the number of cycles
        # print("shape of diff =", np.shape(diff))
        print("shape of x_mean =", np.shape(x_mean))
        print("shape of true_traj =", np.shape(true_traj))
        for VarIndex in range(p.Nx):
            # print("VarINdex =", VarIndex)
            for t in range(0, p.N_c):
                # print("t =", t)
                # print("x_mean[",VarIndex,",", p.L_a*t, "] =", x_mean[VarIndex, p.L_a*t])
                # print("true_traj[", VarIndex, ",", p.L_a*t, "] = ", true_traj[VarIndex, p.L_a * t])
                diff[VarIndex, t] = np.subtract(x_mean[VarIndex, p.L_a*t], true_traj[VarIndex, p.L_a * t])  # Subtract
                # the true state from the average state analysis
        norm = np.zeros(p.N_c)
        mean_std = np.zeros(p.N_c)
        state_ratio = np.zeros(p.N_c)
        for t in range(p.N_c):
            norm[t] = np.linalg.norm(diff[:, t])  # Calculate norm of differences for each state.
            mean_std[t] = np.mean(x_std[:, t])  # Calculate the mean standard deviation of all states
            state_ratio[t] = np.divide(norm[t], p.Nx*mean_std[t])  # Calculate the ratio between the bias and the variance at
            # each time step.
    plt.figure(1)
    plt.subplots(2, 1)  # Plot the number of subplots equal to the number of state variables to plot.
    xc_label = np.arange(0, p.N_c, 5)
    xc = np.arange(0, p.N_c)
    plt.subplot(2, 1, 1)
    plt.plot(xc, state_ratio, 'o-', c='r', label='Ratio of Bias in State')  # Plot analysis ratio
    plt.axhline(y=0.1, color='black', linestyle='--')
    plt.ylabel("$||x^a - x^t||/(40\sigma_{ax})$", fontsize=20)
    plt.xticks(xc_label, size=14)
    plt.yticks(size=14)
    plt.tight_layout()
    plt.legend(loc="lower right", fontsize=16)
    plt.subplot(2, 1, 2)  # Plot the bias coefficient ratio
    if p.L_a >= p.Obs_sep_time[0]:  # If doing 3DVar over one time step or 4DVar over multiple time steps, subtract
        # whole of truth state
        diff_b = np.subtract(b_mean, p.true_b)  # Subtract the true bias coefficient from the average bias coefficient
        # analysis
        bias_ratio = np.abs(np.divide(diff_b, obb))  # Find the ratio between the difference and the bias std
    else:  # Else if doing 3DVar over multiple time steps just take first value in cycle.
        diff_b = np.zeros(p.N_c)
        bias_ratio = np.zeros(p.N_c)
        for c in range(p.N_c):
            diff_b[c] = np.subtract(b_mean[p.L_a * c], p.true_b)  # Subtract the true bias coefficient from the average
            # bias coefficient analysis
            bias_ratio[c] = np.abs(np.divide(diff_b[c], obb))  # Find the ratio between the difference and the bias std
    print("bias_diff =", diff_b)
    # print("bias ratio =", bias_ratio)
    plt.plot(xc, bias_ratio, 'o-', c='m', label='Ratio of Bias in Bias Coefficient')  # Plot bias coefficient ratio
    plt.axhline(y=0.1, color='black', linestyle='--')
    plt.ylabel("$b$", fontsize=20)
    plt.xticks(xc_label, size=14)
    plt.yticks(size=14)
    plt.xlabel("Number of cycles", fontsize=20)
    plt.legend(loc="lower right", fontsize=16)
    plt.tight_layout()
    plt.show()


# plot_diff_error_var()
# true()
# plot_loc_model_bias()
# plot_bias_ratio_vary_Lb()
# plot_bias_ratio_vary_o1o2()
# plot_bias_ratio_vary_o2()
# cycling_ratio()
# model_error()
# an_bkd_error()
cycling_norm()
