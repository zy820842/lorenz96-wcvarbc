import many_realisations_spread as many_real
import Lorenz96
import parameters as p
import numpy as np
from Tests import test_TL, test_ADJ
import pickle

N_realisations = 2 #1000  # number of realisations
BC = 1  # don't/do perform VarBC
WC = 1  # don't/do perform weak-constraint4DVar
ob = 1  # state background observation error variance
sigma_o = [1, 1000]  # observation error variance for biased and unbiased obs
beta_b_obs = [0, 0]  # state background bias relative to both observation types
L_b = 0.2  # Length scale of B

print("N_realisations =", N_realisations)
print("BC =", BC)
print("WC =", WC)
print("sigma_o =", sigma_o)
print("beta_b_obs =", beta_b_obs)
print("model_bias_incr =", p.model_bias_incr)
print("Nx =", p.Nx)
print("F =", p.F)
print("Fa =", p.F_A)
print("dt =", p.dt)
print("L_a =", p.L_a)
print("L_f =", p.L_f)
print("N_c =", p.N_c)
print("beta_b =", p.beta_b)
print("N_ob_types =", p.N_ob_types)
print("shift =", p.shift)
print("Obs_sep_dist =", p.Obs_sep_dist)
print("Obs_sep_time =", p.Obs_sep_time)
print("time_shift =", p.time_shift)
print("beta_o =", p.beta_o)


if BC: 
    print("Nb =", p.Nb)
    print("true_b =", p.true_b)
    print("spatial_var =", p.spatial_var)
    
if WC: 
    print("Ne =", p.Ne)
    print("true_eta =", p.true_eta)


#file_nameB = 'B_Fa_' + str(p.F_A) + '_Nx_' + str(p.Nx)  + '_WC_0_BC_1_Nobtypes_'+ str(p.N_ob_types) +'_Obs_sep_dist_' + str(p.Obs_sep_dist[0]) + '_' + str(p.Obs_sep_dist[1]) + '_La_' + str(p.L_a) + '_Lf_' \
                #+ str(p.L_f) + '_dt_' + str(p.dt) + '_o1_1_o2_1_ob_' + \
                #str(ob) + '_obb_' + str(p.sigma_bbeta)
#Bv = pickle.load(open(file_nameB + ".dat", "rb"))  

#Bvtotal = 0
#for i in range(p.Nx): 
#    Bvtotal += Bv[i, i]
#Bvmean = Bvtotal/p.Nx
#Q = 0.1*Bvmean
#Bp = np.zeros((p.Nx + p.Nb + p.Ne, p.Nx + p.Nb + p.Ne))
#Bp[0:p.Nx + p.Nb, 0:p.Nx + p.Nb] = Bv
#Bp[p.Nx + p.Nb, p.Nx + p.Nb] = Q
#file_nameBp = 'B_Fa_' + str(p.F_A) + '_Nx_' + str(p.Nx)  + '_WC_1_BC_1_Nobtypes_'+ str(p.N_ob_types) +'_Obs_sep_dist_' + str(p.Obs_sep_dist[0]) + '_' + str(p.Obs_sep_dist[1]) + '_La_' + str(p.L_a) + '_Lf_' \
#                + str(p.L_f) + '_dt_' + str(p.dt) + '_o1_1_o2_1_ob_' + \
 #               str(ob) + '_obb_' + str(p.sigma_bbeta)
#pickle.dump(Bp, open(file_nameBp + ".dat", "wb"))  
    
# Specify B matrix with pre-made B, if 0 then calculate B within data assimilation system.
file_name = 'B_Fa_' + str(p.F_A) + '_Nx_' + str(p.Nx)  + '_WC_' + str(WC) + '_BC_' + str(BC) + '_Nobtypes_'+ str(p.N_ob_types) +'_Obs_sep_dist_' + str(p.Obs_sep_dist[0]) + '_' + str(p.Obs_sep_dist[1]) + '_La_' + str(p.L_a) + '_Lf_' \
                + str(p.L_f) + '_dt_' + str(p.dt) + '_o1_1_o2_1_ob_' + \
                str(ob) + '_obb_' + str(p.sigma_bbeta)
B = pickle.load(open(file_name + ".dat", "rb"))
print("shape of B =", np.shape(B))

Tests = 0  # 1/0 do/do not run TL and Adj tests.
if Tests:
	test_TL(WC)
	test_ADJ()


win_len = p.L_a*p.N_c  # window length parameter to be used in plotting figures.
true = Lorenz96.Lorenz96_nonLinear_propogation(p.Ics(), win_len-1, p.F)[0]
norm_true = np.zeros(win_len)  # Initialise a vector to store the normalised true trajectory
for t in range(win_len):
	norm_true[t] = np.linalg.norm(true[:, t])  # Normalise the true trajectory over all states

def run_analysis(WC, BC):
	"""Caculate analysis when wc4Dvar and/or VarBC are used"""
	analysis = many_real.all_real(N_realisations, BC, ob, sigma_o, beta_b_obs, L_b, B, WC)
	return analysis

analysis = run_analysis(WC, BC)  # Run DA for given bias correction methods
state_a = analysis['state_a_total']  # state analysis
state_b = analysis['state_b_total']  # state background
obs = analysis['obs_total_ar']  # observations
statea_file = 'state_analysis_WC_' + str(WC) + '_BC_' + str(BC) +  '_Fa_' + str(p.F_A) + '_true_b_' + str(p.true_b) + '_Obs_time_shift_' + str(p.time_shift[0]) + '_' + str(p.time_shift[1]) + '_o1_' + str(sigma_o[0]) + '_o2_' + str(sigma_o[1])  # Define file name to
# save data as, based on parameters.
obs_file = 'obs_WC_' + str(WC) + '_BC_' + str(BC) +  '_Fa_' + str(p.F_A) + '_true_b_' + str(p.true_b) + '_Obs_time_shift_' + str(p.time_shift[0]) + '_' + str(p.time_shift[1]) + '_o1_' + str(sigma_o[0]) + '_o2_' + str(sigma_o[1])
# Save state_ratio
pickle.dump(state_a, open(statea_file + ".dat", "wb"))  # Save the variables in the file, writing a binary file, ("wb")
pickle.dump(obs, open(obs_file + ".dat", "wb")) 
if WC:
	model_bias_a = analysis['model_a_total']  # model bias estimate analysis
	model_bias_b = analysis['model_b_total']  # model bias estimate background
else:  # if not using weak-constraint set model bias analysis and background to empty
	model_bias_a = []
	model_bias_b = []
if BC:
	obs_bias_a = analysis['bias_a_total']  # observation bias estimate analysis
	obs_bias_b = analysis['bias_b_total']  # observation bias estimate background
else:  # if not using VarBC, set obs bias analysis and background to empty
	obs_bias_a = []
	obs_bias_b = []

if N_realisations != 1:  # if more than one realisation, calculate average of analysis and background for state, obs
	# bias coeff and model bias parameter.
	ave = many_real.ave_and_std(state_a, state_b, obs_bias_a, BC, model_bias_a, WC)  # Calculate averages and stds for
	# the state and the model and obs bias coefficient analyses.
	state_ave = ave['state_ave'][:, 0:win_len]
	state_std = ave['state_std'][:, 0:win_len]
	bkd_ave = ave['bkd_ave'][:, 0:win_len]  # State background average
	bkd_std = ave['bkd_std'][:, 0:win_len]  # State background standard deviation
	norm_ave = np.zeros(win_len)  # initialise a vector to store the norm of all average states.
	mean_std = np.zeros(win_len)  # initialise a vector to store the standard deviations of all states
	state_ratio = np.zeros(win_len)  # initialise a vector to store the ratio between norm and standard deviation of
	# all states
	for t in range(win_len):
		norm_ave[t] = np.linalg.norm(state_ave[:, t])  # Calculate norm of average realisation for each state.
		mean_std[t] = np.mean(state_std[:, t])  # Calculate the mean standard deviation of all states
		state_ratio[t] = np.divide(norm_ave[t], p.Nx*mean_std[t])  # Calculate the ratio between the bias and the
		# variance at each time step.
	if BC:
		bias_ave = ave['bias_ave'][0:win_len]  # obs bias coefficient analysis average
		bias_std = ave['bias_std'][0:win_len]  # obs bias coefficient analysis standard deviation
	if WC:
		mbias_ave = ave['mbias_ave'][0:win_len]  # model bias coefficient analysis average
		mbias_std = ave['mbias_std'][0:win_len]  # model bias coefficient analysis standard deviation

	'''Calculate the ratio between bias and random error in the state, obs bias coefficient and model bias coefficient '''
	diff = np.zeros((p.Nx, win_len))  # Initialise an array to hold the difference values between the true and the ave
	# state for each state at each time.
	for VarIndex in range(p.Nx):
		diff[VarIndex, :] = np.subtract(state_ave[VarIndex, :], true[VarIndex, 0:win_len])  # Subtract the
		# true state from the average state analysis to find the bias in the state analysis
	ave_diff = np.zeros(win_len)
	ave_ratio = np.zeros(win_len)
	norm_diff = np.zeros(win_len)
	state_ratio = np.zeros(win_len)
	for t in range(win_len):  # Plot the ratio between the bias and the random error for each state variable
		norm_diff[t] = np.linalg.norm(diff[:, t])  # Calculate norm of differences for each state.
		state_ratio[t] = np.divide(norm_diff[t], p.Nx * mean_std[t])  # Calculate the ratio between the bias and the
		# variance at each time step.
		ave_diff[t] = np.average(diff[:, t])
		ave_ratio[t] = np.divide(ave_diff[t], mean_std[t])
	if BC:
		diff_beta = np.zeros((p.Nb, win_len))  # Initialise an array to hold the difference values between the true and
		# the ave state for each state at each time.
		for VarIndex in range(p.Nb):
			if np.size(p.true_b) == 1:
				diff_beta[VarIndex, :] = np.subtract(bias_ave[VarIndex, :], p.true_b*np.ones(win_len))  # Subtract the
				# true state from the average state analysis to find the bias in the state analysis
			else:
				print("true_b is not scalar, need to code this")
		norm_diffb = np.zeros(win_len)
		bias_ratio = np.zeros(win_len)
		mean_stdb = np.zeros(win_len)
		for t in range(win_len):  # Plot the ratio between the bias and the random error for each obs bias_coefficient
			norm_diffb[t] = np.linalg.norm(diff_beta[:, t])  # Calculate norm of differences for each bias coefficient.
			mean_stdb[t] = np.mean(bias_std[:, t])  # Calculate the mean standard deviation of all bias coefficients
			bias_ratio[t] = np.divide(norm_diffb[t], p.Nb * mean_stdb[t])  # Calculate the ratio between the bias and the
			# variance at each time step.
	if WC:
		diff_eta = np.zeros((p.Ne, win_len))  # Initialise an array to hold the difference values between the true and
		# the ave state for each state at each time.
		for VarIndex in range(p.Ne):
			if np.size(p.true_eta) == 1:
				diff_eta[VarIndex, :] = np.subtract(mbias_ave[VarIndex, :], p.true_eta*np.ones(win_len))  # Subtract the
				# true state from the average state analysis to find the bias in the state analysis
			else:
				print("true_eta is not scalar, need to code this")
		norm_diffe = np.zeros(win_len)
		mbias_ratio = np.zeros(win_len)
		mean_stde = np.zeros(win_len)
		for t in range(win_len):  # Plot the ratio between the bias and the random error for each model bias_coefficient
			norm_diffe[t] = np.linalg.norm(diff_eta[:, t])  # Calculate norm of differences for each bias coefficient.
			mean_stde[t] = np.mean(mbias_std[:, t])  # Calculate the mean standard deviation of all bias coefficients
			mbias_ratio[t] = np.divide(norm_diffe[t], p.Ne * mean_stde[t])  # Calculate the ratio between the bias and the
			# variance at each time step.

state_file = 'state_ratio_WC_' + str(WC) + '_BC_' + str(BC) +  '_Fa_' + str(p.F_A) + '_true_b_' + str(p.true_b) + '_Obs_time_shift_' + str(p.time_shift[0]) + '_' + str(p.time_shift[1]) + '_o1_' + str(sigma_o[0]) + '_o2_' + str(sigma_o[1])  # Define file name to
# save data as, based on parameters.

# Save state_ratio
pickle.dump(state_ratio, open(state_file + ".dat", "wb"))  # Save the variables in the file, writing a binary file, ("wb")

if BC: 
    obs_file = 'obs_bias_ratio_WC_' + str(WC) + '_BC_' + str(BC) +  '_Fa_' + str(p.F_A) + '_true_b_' + str(p.true_b) +      '_Obs_time_shift_' + str(p.time_shift[0]) + '_' + str(p.time_shift[1]) + '_o1_' + str(sigma_o[0]) + '_o2_' + str(sigma_o[1])  # Define  file name to
    # save data as, based on parameters.
    coeff_file = 'obs_bias_coeff_WC_' + str(WC) + '_BC_' + str(BC) +  '_Fa_' + str(p.F_A) + '_true_b_' + str(p.true_b) +      '_Obs_time_shift_' + str(p.time_shift[0]) + '_' + str(p.time_shift[1]) + '_o1_' + str(sigma_o[0]) + '_o2_' + str(sigma_o[1])
    pickle.dump(bias_ave, open(coeff_file + ".dat", "wb"))
    coeff_std_file = 'obs_bias_coeff_std_WC_' + str(WC) + '_BC_' + str(BC) +  '_Fa_' + str(p.F_A) + '_true_b_' + str(p.true_b) +      '_Obs_time_shift_' + str(p.time_shift[0]) + '_' + str(p.time_shift[1]) + '_o1_' + str(sigma_o[0]) + '_o2_' + str(sigma_o[1])
    pickle.dump(bias_std, open(coeff_std_file + ".dat", "wb"))
    # Save bias_ratio
    pickle.dump(bias_ratio, open(obs_file + ".dat", "wb"))  # Save the variables in the file, writing a binary file, ("wb")

if WC: 
    coeff_file = 'model_bias_coeff_WC_' + str(WC) + '_BC_' + str(BC) +  '_Fa_' + str(p.F_A) + '_true_b_' + str(p.true_b) +      '_Obs_time_shift_' + str(p.time_shift[0]) + '_' + str(p.time_shift[1]) + '_o1_' + str(sigma_o[0]) + '_o2_' + str(sigma_o[1])
    pickle.dump(mbias_ave, open(coeff_file + ".dat", "wb"))
    model_file = 'model_bias_ratio_WC_' + str(WC) + '_BC_' + str(BC) +  '_Fa_' + str(p.F_A) + '_true_b_' + str(p.true_b) + '_Obs_time_shift_' + str(p.time_shift[0]) + '_' + str(p.time_shift[1]) + '_o1_' + str(sigma_o[0]) + '_o2_' + str(sigma_o[1])  # Define file name to
    # save data as, based on parameters.

    # Save mbias_ratio
    pickle.dump(mbias_ratio, open(model_file + ".dat", "wb"))  # Save the variables in the file, writing a binary file, ("wb")



