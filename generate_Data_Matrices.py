#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
generate data and coavariance and observation operator matrices
Created on Wed Jul  3 11:38:28 2019

@author: alisonfowler
"""
from scipy.linalg import circulant, schur
import math
from parameters import Nx, Nb, Ne, sigma_bbeta, beta_b, True_ics, Ics, true_b, true_eta, L_a, F_A, Ob_description, \
    Obs_sep_dist, Obs_kernal_width, Obs_sep_time, beta_o, L_domain, N_ob_types, L_f, F, shift, dt, F_Ics, sigma_beta, time_shift, spatial_var
import numpy as np
from scipy.linalg import pinv
import Lorenz96
import DataAssimilation
import matplotlib.pyplot as plt
import pickle  # pickle module lets you save variables.
import os.path
from matplotlib.colors import TwoSlopeNorm
# np.random.seed()
'''
simulate first guess from truth and true error statistics
'''
def generate_fg(verbosity, BC, initial, trigger, sigma_b, Ob_info, beta_b_obs, L_b, sigma_o, dom_size, b_lower, b_upper,
                n_lower,  cycledB=0, initialcond = Ics(), WC=0):
    ''' generate B matrix'''
    np.random.seed()
    #print('generate B')
    if verbosity:
        print('generate B')
    if np.size(cycledB)==1:  # If a B hasn't been specified, we need to calculate B using the SOAR function.
        B = np.zeros((dom_size, dom_size))
        if initial:  # If on initial cycle used to determine a true B, use SOAR function to generate covariance
            # matrix.
            B[0:Nx, 0:Nx] = generate_cov(sigma_b, L_b, Nx)  # First Nx x Nx elements are for the state, so are
            # calculated as usual.
            if BC:
                B[b_lower:b_upper, b_lower: b_upper] = sigma_bbeta * np.identity(Nb)  # Define bias background error
                # covariance matrix to be the identity multiplied by bias coefficient bkd variance.
            if WC:
                B[n_lower:dom_size, n_lower: dom_size] = sigma_beta * np.identity(Ne)  # Define bias background error
                # covariance matrix to be the identity multiplied by model bias parameter bkd variance.
        else:  # If on actual assimilation then use B calculated from initial ensemble.
            B = new_B(BC, trigger, sigma_b, Ob_info, beta_b_obs, L_b, sigma_o, dom_size, b_lower, b_upper, n_lower, WC)
    else:  # If B has been previously specified, use the B that was given
        B = cycledB
    # print("size of B =", np.shape(B))
    trigger=0
    # print("plot B")
    if trigger:  # If B hasn't already been plotted, plot B
        norm = TwoSlopeNorm(vmin=-B.max(), vcenter=0, vmax=B.max())
        im = plt.imshow(B, norm=norm, origin='upper', cmap='seismic')  # Plot B as a heatmap.
        cb = plt.colorbar(im)
        cb.set_label(label='$B$', size='xx-large')
        plt.xticks(np.arange(0, dom_size))
        plt.yticks(np.arange(0, dom_size))
        if BC:
            plt.xlabel('States: x0 - x' + str(Nx - 1) + ', Bias Correction Coefficient: x' + str(Nx), fontsize=16)
            plt.ylabel('States: x0 - x' + str(Nx - 1) + ', Bias Correction Coefficient: x' + str(Nx), fontsize=16)
            if WC:
                plt.xlabel('States: x0 - x' + str(Nx - 1) + ', Bias Correction Coefficient: x' + str(Nx) + 
                           ', Model Bias Estimate: n' + str(Nx + Nb), fontsize=20)
                plt.ylabel('States: x0 - x' + str(Nx - 1) + ', Bias Correction Coefficient: x' + str(Nx) + 
                           ', Model Bias Estimate: n' + str(Nx + Nb), fontsize=20)
        elif WC:
            plt.xlabel('States: x0 - x' + str(Nx - 1) + ', Model Bias Parameter: x' + str(Nx), fontsize=20)
            plt.ylabel('States: x0 - x' + str(Nx - 1) + ', Model Bias Parameter: x' + str(Nx), fontsize=20)
        else:
            plt.xlabel('States: x0 - x' + str(Nx - 1), fontsize=20)
            plt.ylabel('States: x0 - x' + str(Nx - 1), fontsize=20)
        plt.show()
        plt.savefig('B.png')
    # print("B =", B)
    print("shape of B in generate_fg =", np.shape(B))
    invB = pinv(B)
    # print("invB =", invB)
    #print('B:',B)
    ''' draw sample from N(beta_b,B)'''
    # eta_b = np.random.normal(beta_b*np.ones(Nx), B[0:Nx, 0:Nx])
    eta_b = np.random.multivariate_normal(beta_b*np.ones(Nx), B[0:Nx, 0:Nx], 1)  # Error in state background (random
    # error around background bias to include potential background bias as well as random error)
    if BC:  # If using bias correction, include error term for bias background.
        eta_beta_b = np.random.multivariate_normal(0*np.ones(Nb), B[b_lower:b_upper, b_lower:b_upper], 1)  # Error in
        # obs bias coeff background (random error only)
    if WC:  # If using wc4dvar include error term for model bias estimate.
        eta_n_b = np.random.multivariate_normal(0*np.ones(Ne), B[n_lower:dom_size, n_lower:dom_size], 1)  # Error in
        # model bias parameter background (random error only)
    ''' Add bias to states observed by biased and unbiased observations.'''
    b = np.zeros(Nx)  # Add bias to each state depending on whether they are observed by each observation.
    H1 = Ob_info[0]['H'][0:Nx, 0:Nx]  # Define observation operator for first type of observation
    bb1 = beta_b_obs[0]  # Define model bias added to states observed by first observation
    H1size = np.size(H1[:, 0])  # Dimension of H1 in number of observations it observes.
    for j in range(H1size):
        for i in range(Nx):
            if H1[j, i] == 1:  # If observation observes state
                b[i] = bb1
    if N_ob_types == 2:
        H2 = Ob_info[1]['H'][0:Nx, 0:Nx]
        H2size = np.size(H2[:, 0])
        bb2 = beta_b_obs[1]  # Define model bias added to states observed by second observation
        for j in range(H2size):
            for i in range(Nx):
                if H2[j, i] == 1: # If observation observes state
                    b[i] += bb2
    '''simulate first guess'''
    fg = initialcond + eta_b + b
    if BC:  # If using bias correction, create a first guess for the bias correction coefficient, beta.
        fg_beta = true_b + eta_beta_b
        # Combine fg and fg_beta so that fg is now of size Nx + Nb
        fg = np.append(fg, fg_beta, axis=1)
    if WC:
        fg_eta = true_eta + eta_n_b  # truth plus random error
        fg = np.append(fg, fg_eta)

    ''' propagate first guess to end of first assimilation cycle '''
    if WC:  # if using weak-constraint, include eta in nonlinear propagation
        traj = Lorenz96.Lorenz96_nonLinear_propogation(fg.flat[0:Nx], L_a, F_A, fg.flat[n_lower: dom_size], WC)
    else:  # otherwise, eta is set to 0
        traj = Lorenz96.Lorenz96_nonLinear_propogation(fg.flat[0:Nx], L_a, F_A, WC)
    fg_traj = traj[0]  # state propagation
    if BC:  # If using bias correction, evolve beta to end of first assimilation cycle.
        fg_beta_traj = fg_beta*np.ones(1 + L_a)
        # Combine fg_traj and fg_beta_traj so that fg_traj is now of size (Nx + Nb, La)
        fg_traj = np.append(fg_traj, fg_beta_traj, axis=0)
    if WC:
        fg_eta_traj = traj[1]  # eta propagation
        fg_traj = np.append(fg_traj, fg_eta_traj, axis=0)
    dict={}
    dict = {'fg':fg, 'B':B, 'invB':invB, 'fg_traj':fg_traj}  # NOTE: fg and fg_traj are now size (Nx+Nb, La) if BC used
    
    return dict

    

'''
simulate observations throughout assimilation window from truth, observation 
operator and true error statistics
'''
def generate_obs(Truth,OType_index,verbosity, BC, bias, N_c, sigma_o, WC):
    """BC = 1/0 if you are/aren't using bias correction. Bias=1/0 if there is/isn't a bias in the observation"""
    np.random.seed()
    if verbosity:
      print('---- ob type:',OType_index, Ob_description[OType_index],'-------')
    ''' generate observation operator- constant for each ob time'''
    if shift != 0:  # Shift observations so that they don't observe the same points.
        H, Ny = generate_H_shift(Obs_sep_dist[OType_index], OType_index, shift)  # Calculate H by permuting the
        # observations, such that they do not observe the same points.
    else:
        H, Ny = generate_H(Obs_sep_dist[OType_index],Obs_kernal_width[OType_index])  # Observation operator if we're not
        # bias correcting and if observations overlap.
    Ny = int(Ny)
    if BC:
        # Generate bias correction
        cx = generate_Cx(Obs_sep_dist[OType_index], Ny)
        cb = generate_Cb(Obs_sep_dist[OType_index], bias, Ny)
        # Add H and Cx to find linearised obs operator in terms of x.
        Hx = np.add(H, cx)
        # Combine Hx and cb to give linearised obs operator in terms of v.
        H = np.append(Hx, cb, axis=1)
    if L_a >= Obs_sep_time[OType_index]:  # If the assimilation length is greater than the distance between observations
        # in time (i.e. using 3DVar with one time step or 4DVar).
        num_ob_times = math.ceil((N_c * L_a) / Obs_sep_time[OType_index])
        ob_times = np.arange(time_shift[OType_index], N_c * L_a + 1, Obs_sep_time[OType_index])
    else:  # If the assimilation length is smaller than the distance between observations in time - i.e. use 3DVar with
        # multiple time steps - then only want observations at initial time step.
        num_ob_times = N_c
        ob_times = np.arange(0, N_c*L_a, L_a)  # Observation times at beginning of each cycle
    ''' draw sample from N(beta_o,sigma_o*Identity)'''
    R = sigma_o[OType_index]*np.identity(Ny) # observation errors are uncorrelated
    ''' simulate observations'''
    obs = np.empty([Ny, num_ob_times])
    if spatial_var == 1: 
        beta_vec = 2*np.sin(np.linspace(0, np.pi, Ny))
    else: 
        beta_vec = np.ones(Ny)
    for tindex in range(num_ob_times):
        eta_o = np.random.multivariate_normal(beta_o[OType_index]*beta_vec, R,).T
        obs[:, tindex] = np.matmul(H[0:Nx, 0:Nx], Truth[:, ob_times[tindex]]) + eta_o
        del eta_o
    invR = pinv(R)
    dict = {}
    dict = {'obs': obs, 'H': H, 'Ny': Ny, 'ob_times': ob_times, 'R': R, 'invR': invR}
        
    return dict


''' generate Ob operator'''
def generate_H(sepdist,kernal_width):
    Ny=math.ceil(Nx/sepdist)
    H=np.zeros([Ny,Nx])
    w_fn=np.zeros([kernal_width,])
    y_loc=L_domain*np.arange(0,Nx,sepdist)/Nx
    y_gp=(Nx*y_loc/L_domain)
        
    ''' define H matrix based on weighting function (assume triangular weighting function)'''
    mid_pt=np.ceil(kernal_width/2)
    w_fn[:]=(mid_pt-np.abs(np.arange(kernal_width)+1-mid_pt))/(mid_pt)
        
    for i in range(Ny):
        if kernal_width==1:
            gp_range=int(y_gp[i])
        elif kernal_width>1:
            gp_range = np.arange(int(y_gp[i]-np.floor(kernal_width/2)),int(y_gp[i]+np.floor(kernal_width/2))+1)
            gp_range[gp_range<0] = Nx+gp_range[gp_range<0]
            gp_range[gp_range>Nx-1] = gp_range[gp_range>Nx-1]-Nx
        H[i,gp_range]=w_fn
   
    return H, Ny


def generate_H_shift(sepdist, OType_Index, shift):
    """Generate the linearised observation operator if the observations do not spatially overlap."""
    Ny = math.ceil(Nx / sepdist)  # Max number of observations
    H = np.zeros((Ny, Nx))
    N = 0  # Count number of observations
    for i in range(Ny):
        if i*sepdist + OType_Index < Nx:
            N += 1  # Count number of observations
            H[i, i*sepdist + OType_Index*shift] = 1
    if N!=Ny:  # If the true number of observations is equal to the max number of observations
        H = np.delete(H, Ny - 1, 0)  # Delete final row
    return H, N

'''generate bias correction'''
def generate_Cx(sepdist, Ny):
    """Generate linearised bias correction in terms of x, sepdist determines how many observations there are in space."""
    cx = 0 * np.ones((Ny, Nx))  # cx is a matrix of length (no. of bias corrected obs x size of state)
    return cx


def generate_Cb(sepdist, bias, Ny):
    """Generate linearised bias correction in terms of beta, sepdist determines how many observations there are in space
    and bias determines if the observations have bias.."""
    if bias:  # If observation has bias, then bias correct every observation.
        cb = np.ones((Ny, 1))  # cb is a matrix of length (no. of bias corrected obs x size of bias correction coeff)
    else:  # If observation has no bias, then define bias correction to be zero.
        cb = np.zeros((Ny, 1))
    return cb

    
    
'''
generate covariance matrix, assuming it is cyclic and correlations follow a SOAR correlation fn
'''
def generate_cov(sigma,L,N):
    """ sigma is error covariance, L is lengthscale and N is size of domain"""
    ''' create SOAR correlation function '''
    N_half = math.floor(N/2.)
    c_b=np.empty([N,])  # Correlation
    c_b[0]=1;
    if L != 0:  # If the length scale is nonzero, calculate covariances via SOAR function
        for k in range(1,N_half+1):
            c_b[k]=(1+(k)/L)*np.exp(-k/L)
        for k in range(0,N_half-1):
            c_b[k+N_half+1]=c_b[N_half-k-1]
        M = sigma*sigma*circulant(c_b)
    else:  # If L is zero then calculate diagonal matrix.
        M = np.zeros((N, N))
        for i in range(N):
            M[i, i] = sigma*sigma
    return M


def cycled_B(BC, sigma_b, Ob_info, beta_b_obs, L_b, sigma_o, dom_size, b_lower, b_upper, n_lower, WC):
    """Repeat the calculation of B for 2-3 time steps to find the optimum B."""
    N_cycles = 3  # Repeat calculation of B N_cycles times.
    assim_cycles = 700  # Number of cycles to do assimilation over.
    true_traj = Lorenz96.Lorenz96_nonLinear_propogation(True_ics(),(L_a+1)*assim_cycles+L_f-1,F)[0]  # Calculate true
    # trajectory
    '''Calculate initial B''' 
    B = np.zeros((dom_size, dom_size))
    print("n_lower =", n_lower)
    print("dom_size =", dom_size)
    print("size of B in cycled B =", np.shape(B))
    # Covariances between bias and state are zero, so block off diagonals can be left as zero.
    B[0:Nx, 0:Nx] = generate_cov(sigma_b, L_b,
                                 Nx)  # First Nx x Nx elements are for the state, so are calculated
    # as usual.
    if BC:
        B[b_lower:b_upper, b_lower: b_upper] = sigma_bbeta * np.identity(Nb)  # Define obs bias coefficient background
        # error covariance matrix to be the identity multiplied by the bias bkd error variance.
    if WC:
        B[n_lower:dom_size, n_lower: dom_size] = sigma_beta * np.identity(Ne)  # Define model bias parameter background
        # error covariance matrix to be the identity multiplied by the bias bkd error variance
    norm = TwoSlopeNorm(vmin=-B.max(), vcenter=0, vmax=B.max())
    im = plt.imshow(B, norm=norm, origin='lower', cmap='seismic')  # Plot B as a heatmap.
    cb = plt.colorbar(im)
    cb.set_label(label='$B$ before multiplying', size='xx-large')
    plt.xticks(np.arange(0, dom_size))
    plt.yticks(np.arange(0, dom_size))
    if BC:
        if WC:
            plt.xlabel('States: x0 - x' + str(Nx - 1) + ', Bias Correction Coefficient: x' + str(Nx) +
                       ', Model bias estimate: x' + str(Nx + Nb), fontsize=20)
            plt.ylabel('States: x0 - x' + str(Nx - 1) + ', Bias Correction Coefficient: x' + str(Nx) +
                       ', Model bias estimate: x' + str(Nx + Nb), fontsize=20)
        else:
            plt.xlabel('States: x0 - x' + str(Nx - 1) + ', Bias Correction Coefficient: x' + str(Nx), fontsize=20)
            plt.ylabel('States: x0 - x' + str(Nx - 1) + ', Bias Correction Coefficient: x' + str(Nx), fontsize=20)
    else:
        if WC:
            plt.xlabel('States: x0 - x' + str(Nx - 1) + ', Model bias estimate: x' + str(Nx + Nb), fontsize=20)
            plt.ylabel('States: x0 - x' + str(Nx - 1) + ', Model bias estimate: x' + str(Nx + Nb), fontsize=20)
        else:
            plt.xlabel('States: x0 - x' + str(Nx - 1), fontsize=20)
            plt.ylabel('States: x0 - x' + str(Nx - 1), fontsize=20)
    plt.show()
    IC = Ics()
    plt.savefig('Binitial_o1-' +str(sigma_o[0]) + '_ob_' + str(sigma_b) + '_Lb-' + str(L_b) + '.png')

    for n in range(N_cycles):
        filename = 'B_Fa' + str(F_A) + '_obs-dist' +str(Obs_sep_dist[0]) + '_La-' + str(L_a) + '_Lb-' + str(L_b) + '_c-' + str(assim_cycles) + '_iter-' + str(n) 
        if os.path.isfile(filename + '.dat'):
            B = pickle.load(open(filename + ".dat", "rb"))  # Load previuos iteration for state from previously saved
            # data file, "rb" stands for read binary file.
        else: 
            an_error = first_cycle(BC, sigma_b, Ob_info, beta_b_obs, L_b, sigma_o, true_traj, assim_cycles, B, IC, dom_size,
                                   b_lower, b_upper, n_lower, WC)
            # Calculate analysis error for every cycle with given B.
            ave_x = an_error['average_x']  # average state analysis at the end of the cycle
            x_a = an_error['error_a_total']  # state analysis error
            B_x = np.cov(x_a[:, :])  # Calculate the covariance matrix of the analysis state at the final time in
            # # the cycle. np.cov(m) where each row of m represents a variable and each column is a single observation of all
            # # those variables.
            soar = np.array(generate_cov(1, 2, Nx))  # Create a SOAR matrix to multiply the old B matrix with.
            B_xnew = np.array(B_x)*soar  # Do element-wise multiplication on the old B matrix so that only values close to
            # the diagonal remain.
            B = np.zeros((dom_size, dom_size))  # Initialise background error covariance matrix to hold both
            # state and bias background values. Covariances between state and bias assumed to be 0.
            B[0:Nx, 0:Nx] = B_xnew  # State part of B
            if BC:
                np.append(ave_x, an_error['average_b'])  # Include average bias analysis at the end of the cycle in the
                # array for the average state analysis.
                B_b = sigma_bbeta  # Define Bb to just be the given error variance for beta.
                B[b_lower:b_upper, b_lower:b_upper] = B_b  # Obs bias coeff part of B
            if WC:
                B_n = sigma_beta  # Define Bn to just be the given error variance for eta.
                B[n_lower:dom_size, n_lower: dom_size] = B_n  # B for model bias parameter
            print("Plot Bx from analysis")
            norm = TwoSlopeNorm(vmin=-B_x.max(), vcenter=0, vmax=B_x.max())
            im = plt.imshow(B_x, norm=norm, origin='lower', cmap='seismic')  # Plot B as a heatmap.
            cb = plt.colorbar(im)
            cb.set_label(label='$B$ old', size='xx-large')
            plt.xticks(np.arange(0, Nx))
            plt.yticks(np.arange(0, Nx))
            plt.xlabel('States: x0 - x' + str(Nx - 1), fontsize=20)
            plt.ylabel('States: x0 - x' + str(Nx - 1), fontsize=20)
            plt.show()
            plt.savefig('Bold_Fa' + str(F_A) + '_obs-dist' +str(Obs_sep_dist[0]) + '_La-' + str(L_a) + '_Lb-' + str(L_b) + '_m-100_c-' + str(assim_cycles) + '.png')
            plt.clf()
            print(n, "th cycle")
            # Plot B
            print("Plot B after multiplied by SOAR")
            norm = TwoSlopeNorm(vmin=-B.max(), vcenter=0, vmax=B.max())
            im = plt.imshow(B, norm=norm, origin='lower', cmap='seismic')  # Plot B as a heatmap.
            cb = plt.colorbar(im)
            cb.set_label(label='$B$ new', size='xx-large')
            plt.xticks(np.arange(0, dom_size))
            plt.yticks(np.arange(0, dom_size))
            if BC:
                plt.xlabel('States: x0 - x' + str(Nx - 1) + ', Bias Correction Coefficient: x' + str(Nx), fontsize=20)
                plt.ylabel('States: x0 - x' + str(Nx - 1) + ', Bias Correction Coefficient: x' + str(Nx), fontsize=20)
            else:
                plt.xlabel('States: x0 - x' + str(Nx - 1), fontsize=20)
                plt.ylabel('States: x0 - x' + str(Nx - 1), fontsize=20)
            plt.show()
            plt.savefig('Bnew_Fa' + str(F_A) + '_obs-dist' +str(Obs_sep_dist[0]) + '_La-' + str(L_a) + '_Lb-' + str(L_b) + '_m-100_c-' + str(assim_cycles) + '_iter-' + str(n) +  '.png')
            plt.clf()
            pickle.dump(B, open(filename + ".dat", "wb"))  # Save B for each iteration
    file_name = 'B_Fa_' + str(F_A) + '_Nx_' + str(Nx) + '_WC_' + str(WC) + '_BC_' + str(BC) + '_Nobtypes_'+ str(N_ob_types)  \
                +'_Obs_sep_dist_' + str(Obs_sep_dist[0]) + '_' + str(Obs_sep_dist[1]) + '_La_' + str(L_a) + '_Lf_' \
                + str(L_f) + '_dt_' + str(dt) + '_o1_' + str(sigma_o[0]) + '_o2_' + str(sigma_o[1]) + '_ob_' + \
                str(sigma_b) + '_obb_' + str(sigma_bbeta)  # Define file name to
    # save data as, based on parameters.

    # Save B
    pickle.dump(B, open(file_name + ".dat", "wb"))  # Save the variables in the file, writing a binary file, ("wb")
    print("shape of B in cycled_b =", np.shape(B))
    return B


def first_cycle(BC, sigma_b, Ob_info, beta_b_obs, L_b, sigma_o, true_traj, N_cycles, B, IC, dom_size, b_lower, b_upper,
                n_lower, WC):
    '''Calculate first cycle to be used to produce a better background error covariance matrix. N_c = 1. '''
    N_realisations = 15  # number of realisations to average results over
    forced_traj = Lorenz96.Lorenz96_nonLinear_propogation(True_ics(),(L_a+1)*N_cycles+L_f-1,F_A)  # Calculate true
    # trajectory
    error_a_length = L_a * N_cycles
    a_total = np.zeros((Nx, error_a_length*N_realisations))  # Initialise array to store state analysis for
    # every realisation.
    a_real_total = np.zeros((Nx, error_a_length, N_realisations))  # Initialise array to store state analysis for
    # every state, time step and realisation. (Used in calculating average analysis).
    average_x = np.zeros(Nx)  # Initialise array to store average state analysis over all realisations for the final
    # time step
    if L_a>=Obs_sep_time[0]:  # If the assimilation length is greater than the distance between observations in
        # time (ie. using 3DVar with one time step or 4DVar).
        error_a_total = np.zeros((Nx, error_a_length*N_realisations))
    else:
        error_a_total = np.zeros((Nx, N_cycles * N_realisations))
    if BC:
        b_total = np.zeros((Nb, error_a_length*N_realisations))  # Initialise array to store bias analysis
        # for every realisation.
        average_b = np.zeros(Nb)  # Initialise array to store average bias analysis over all realisations for the final
        # time step.
        if L_a>=Obs_sep_time[0]:  # If the assimilation length is greater than the distance between observations in
        #         # time (ie. using 3DVar with one time step or 4DVar).
            error_b_total = np.zeros((Nb, error_a_length*N_realisations))   # Initialise array to store differnce
        # between bias analysis and truth for all realisations.
        else:
            # error_b_total = np.zeros((Nb, (N_cycles + 1)*N_realisations))# Initialise array to store differnce
        # between bias analysis and truth for all realisations.
            error_b_total = np.zeros((Nb, N_cycles * N_realisations))
    if WC:
        n_total = np.zeros((Ne, error_a_length*N_realisations))  # Initialise array to store model bias analysis
        # for every realisation.
        average_n = np.zeros(Ne)  # Initialise array to store average model bias analysis over all realisations for the
        # final time step.
        if L_a>=Obs_sep_time[0]:  # If the assimilation length is greater than the distance between observations in
        #         # time (ie. using 3DVar with one time step or 4DVar).
            error_n_total = np.zeros((Ne, error_a_length*N_realisations))   # Initialise array to store differnce
        # between bias analysis and truth for all realisations.
        else:
            # error_b_total = np.zeros((Nb, (N_cycles + 1)*N_realisations))# Initialise array to store differnce
        # between bias analysis and truth for all realisations.
            error_n_total = np.zeros((Ne, N_cycles * N_realisations))
    plot=1  # Plot realisation for first realisation
    for nr in range(N_realisations):  # Calculate analysis for each realisation
        print('performing realisation ' + str(nr + 1) + ' of ' + str(N_realisations), ' for initial cycle')

        #################################################################
        # 2. generate first guess
        #################################################################
        print('********** generating first guess for initial cycle************')
        initial=1
        trigger=0
        print("shape of B in first_cycle =", np.shape(B))
        bg_info = generate_fg(1, BC, initial, trigger, sigma_b, Ob_info, beta_b_obs, L_b, sigma_o, dom_size, b_lower, b_upper, n_lower, B, IC, WC)  # Use the
        # B and the initial conditions that were calculated from the previous analysis.
        #################################################################
        # 3. generate obs
        #################################################################
        print('********** generating observations for initial cycle************')
        Ob_info = [''] * N_ob_types
        # print("Ob_info =", Ob_info)
        for ob_type_index in range(N_ob_types):
            if beta_o[ob_type_index] == 0:
                # If ob_type is considered unbiased, do not use bias correction.
                print("###No bias in observations: if bias correcting, set bias correction to be zero.###")
                Ob_info[ob_type_index] = generate_obs(true_traj, ob_type_index, 1, BC, 0, N_cycles, sigma_o, WC)
            else:  # If ob_type is biased, use bias correction when creating H
                print("###Bias in observations: if bias correcting, include bias correction in observation operator.###")
                Ob_info[ob_type_index] = generate_obs(true_traj, ob_type_index, 1, BC, 1, N_cycles, sigma_o, WC)

        #################################################################
        # 4. perform DA
        #################################################################
        Xa_traj, J, dJ, normdJ, Ny_window, x, arg = DataAssimilation.var4d_cycled(bg_info, Ob_info, 1, BC, N_cycles,
                                                                                  sigma_o, dom_size, b_lower,
                                                                                  b_upper, n_lower, WC)
        #################################################################
        # 5. generate forecast
        # #################################################################
        # print("shape of Xa_traj =", np.shape(Xa_traj))
        # print("La*N0cycles =", L_a*N_cycles)
        if WC:  # if using weak-constraint, include eta in nonLinear propagation
            fcst_full = Lorenz96.Lorenz96_nonLinear_propogation(Xa_traj[0:Nx, L_a * N_cycles - 1], L_f - 1, F_A,
                                                            Xa_traj[n_lower, dom_size], WC)  # Calc both state and model
            # bias estimate propagation.
        else: # otherwise, set eta to 0
            fcst_full = Lorenz96.Lorenz96_nonLinear_propogation(Xa_traj[0:Nx, L_a * N_cycles - 1], L_f - 1, F_A, WC)
        fcst = fcst_full[0]
        if BC:  # If using bias correction, generate bias forecast by propagating forward.
            fcst = np.append(fcst, Xa_traj[Nx:Nx + Nb,L_a*N_cycles-1]*np.ones((1, L_f)), axis=0)
        if WC:  # If using weak-constraint, generate model bias parameter forecast by propagating forward.
            fcst = np.append(fcst, fcst_full[1], axis=0)
        analysis_fcst = np.concatenate((Xa_traj, fcst[:, 1:L_f]), axis=1)[:, 0:L_a*N_cycles]  # Forecast for next L_f
        # time steps.
        a_total[:, nr*error_a_length:nr*error_a_length+error_a_length] = analysis_fcst[0:Nx, :]  # Store each state
        # analysis realisation in the total array.
        a_real_total[:, :, nr] = analysis_fcst[0:Nx, :]
        if BC:
            b_total[:, nr*error_a_length:nr*error_a_length+error_a_length] = analysis_fcst[b_lower:b_upper, :]  # Store
            # each bias analysis realisation in total array.
        if WC:
            n_total[:, nr*error_a_length:nr*error_a_length+error_a_length] = analysis_fcst[n_lower:dom_size, :]  # Store
            # each bias analysis realisation in total array.
    # Calculate average analysis value over all realisations at each time step.
    ave_a = np.zeros((Nx, error_a_length))  # Initialise array to store the average analysis for all states at all times
    # over all realisations.
    for i in range(Nx):
        for j in range(error_a_length):
            ave_a[i, j] = np.mean(a_real_total[i, j, :])  # Calculate average of all realisations for each state and
            # time step.
    for nr in range(N_realisations):  # Calculate error between analysis and average for each realisation.
        if L_a >= Obs_sep_time[0]:  # If the assimilation length is greater than the distance between observations in
        # time (ie. using 3DVar with one time step or 4DVar).
            for t in range(error_a_length):
                error_a_total[:, nr*error_a_length + t] = np.subtract(a_total[:, nr*error_a_length + t], ave_a[:, t])# forced_traj[:, t]) #true_traj[:, t])
                # Calculate the error of the state analyis between the realisation and the average analsis of all
                # realisations.
                if BC:
                    error_b_total[:, nr*error_a_length + t] = np.subtract(b_total[:, nr*error_a_length + t], true_b)
                    # Calculate the error of the bias
                if WC:
                    error_n_total[:, nr*error_a_length + t] = np.subtract(n_total[:, nr*error_a_length + t], true_eta)
                    # Calculate the error of the bias
        else:
            for t in range(N_cycles):
                error_a_total[:, N_cycles*nr + t] = np.subtract(a_total[:, N_cycles*L_a*nr + (t + 1)*L_a - 1], ave_a[:, (t + 1)*L_a - 1])
                # Calculate the error of the state analyis between the realisation and the true bias at the end of the
                # cycle
                if BC:
                    # Calculate the error of the bias
                    error_b_total[:, N_cycles*nr + t] = np.subtract(b_total[:, N_cycles*L_a*nr + (t + 1)*L_a - 1],
                                                                          true_b)
                    error_n_total[:, N_cycles*nr + t] = np.subtract(n_total[:, N_cycles*L_a*nr + (t + 1)*L_a - 1],
                                                                          true_eta)
    a_end = np.zeros((Nx, N_realisations))  # Initialise an array to store the final analysis values of all realisations.
    if BC:
        b_end = np.zeros((Nb, N_realisations))  # Initialise an array to store the final bias analysis values of all
        # reals.
    if WC:
        n_end = np.zeros((Nb, N_realisations))  # Initialise an array to store the final bias analysis values of all
        # reals.
    for nr in range(N_realisations):
        # Store the final analysis value in the cycle for each realisation to find the average final analysis. This will
        # be used as the new initial conditions for the next cycle.
        a_end[:, nr] = a_total[:, nr*error_a_length + error_a_length - 1]
        if BC:
            b_end[:, nr] = b_total[:, nr*error_a_length + error_a_length - 1]
            if WC:
                n_end[:, nr] = n_total[:, nr*error_a_length + error_a_length - 1]
    for i in range(Nx):
        average_x[i] = np.mean(a_end[i, :])  # Find the mean value of the state analysis at the end of the cycle over
        # all realisations.
    if BC:
        for i in range(Nb):
            average_b[i] = np.mean(b_end[i, :])  # Find the mean value of the bias analysis at the end of the cycle over
            # all realisations.
    if WC:
        for i in range(Ne):
            average_n[i] = np.mean(n_end[i, :])  # Find the mean value of the model bias analysis at the end of the
            # cycle over all realisations.

    dict = {'error_a_total':error_a_total, 'average_x': average_x}
    if BC:
        dict['error_b_total'] = error_b_total
        dict['average_b'] = average_b
    if WC:
        dict['error_n_total'] = error_n_total
        dict['average_n'] = average_n
    return dict


def new_B(BC, trigger, sigma_b, Ob_info, beta_b_obs, L_b, sigma_o, dom_size, b_lower, b_upper, n_lower, WC):
    '''Calculate background values from the background error covariance matrix produced by the initial cycle of
    realisations.  BC determines whether to use bias correction in initial cycle, trigger determines whether to plot
    B matrix (only plot on first realisation).'''
    # Check if file already exists, to reduce computation time.
    file_name_x = 'B_Fa_' + str(F_A) + '_Nx_' + str(Nx)  + '_WC_' + str(WC) + '_BC_' + str(BC) + '_Nobtypes_'+ str(N_ob_types)  \
                +'_Obs_sep_dist_' + str(Obs_sep_dist[0]) + '_' + str(Obs_sep_dist[1]) + '_La_' + str(L_a) + '_Lf_' \
                + str(L_f) + '_dt_' + str(dt) + '_o1_' + str(sigma_o[0]) + '_o2_' + str(sigma_o[1]) + '_ob_' + \
                str(sigma_b) + '_obb_' + str(sigma_bbeta)
    print("file_name_x =", file_name_x)
    if os.path.isfile(file_name_x + '.dat'):
        B = pickle.load(open(file_name_x + ".dat", "rb"))  # Load initial cycle for state from previously saved
        # data file, "rb" stands for read binary file.
        print("file exists")
    else:   # If file does not exist then run first_cycle to recover the total error.
        print("file does not exist")
        B = cycled_B(BC, sigma_b, Ob_info, beta_b_obs, L_b, sigma_o, dom_size, b_lower, b_upper, n_lower, WC)
        print("shape of B in new_B =", np.shape(B))
    trigger = 0
    if trigger:  # If B hasn't already been plotted, then plot B
        norm = TwoSlopeNorm(vmin=-B.max(), vcenter=0, vmax=B.max())
        im = plt.imshow(B, norm=norm, origin='lower', cmap='seismic')  # Plot B as a heatmap.
        cb = plt.colorbar(im)
        cb.set_label(label='$B$', size='xx-large')
        plt.xticks(np.arange(0, dom_size))
        plt.yticks(np.arange(0, dom_size))
        if BC:
            if WC:
                plt.xlabel('States: x0 - x' + str(Nx - 1) + ', Bias Correction Coefficient: x' + str(Nx) +
                           'Model bias estimate' + str(Nx + Nb), fontsize=20)
                plt.ylabel('States: x0 - x' + str(Nx - 1) + ', Bias Correction Coefficient: x' + str(Nx) +
                           'Model bias estimate' + str(Nx + Nb), fontsize=20)
            else:
                plt.xlabel('States: x0 - x' + str(Nx- 1) + ', Bias Correction Coefficient: x' + str(Nx), fontsize=20)
                plt.ylabel('States: x0 - x' + str(Nx- 1) + ', Bias Correction Coefficient: x' + str(Nx), fontsize=20)
        else:
            if WC:
                plt.xlabel('States: x0 - x' + str(Nx - 1) + 'Model bias estimate' + str(Nx), fontsize=20)
                plt.ylabel('States: x0 - x' + str(Nx - 1) + 'Model bias estimate' + str(Nx), fontsize=20)
            else:
                plt.xlabel('States: x0 - x' + str(Nx - 1), fontsize=20)
                plt.ylabel('States: x0 - x' + str(Nx - 1), fontsize=20)
        plt.show()
    return B
