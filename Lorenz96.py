#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Lorenz 96 non-linear model, and TL and ADJ
The Lorenz 1996 model is cyclical: dx[j]/dt=(x[j+1]-x[j-2])*x[j-1]-x[j]+F

Created on Wed Jul  3 10:46:32 2019

@author: alisonfowler
"""
import numpy as np
from parameters import dt,Nx, Ne
''' create a vector of eta the same length as the state
    could add in more options later
'''
def eta_vector(eta):
    if Ne == Nx:
        eta_v = eta
    elif Ne == 1:
        eta_v = eta*np.ones((Nx))
    return eta_v


def eta_vector_ADJ(eta_v_hat):
    eta_hat = 0
    if Ne == Nx:
        eta_hat = eta_v_hat
    elif Ne == 1:
        for i in range(Nx):
            eta_hat = eta_hat + eta_v_hat[i]
    return eta_hat


'''
# Non linear model
'''


def Lorenz96_nonLinear_propogation(x0, Sim_length, force, eta=0, WC=0):
    """x0 is shape Nx, eta is shape Ne"""
    if WC:
        eta_Nx = eta_vector(eta)
        NL_eta_traj = np.empty([Ne, Sim_length + 1])
        NL_eta_traj[:, 0] = eta
    else:
        eta_Nx = np.zeros((Nx,))
        NL_eta_traj = np.zeros([Ne, Sim_length + 1])
    NL_traj = np.empty([Nx, Sim_length+1])
    NL_traj[:, 0] = x0.flat
       
    xf_old = x0.flat
    
    for tstep in range(Sim_length):
        xtmp = xf_old
        
        k1 = lorenz_main(xtmp, force)
        xtmp = xf_old+0.5*k1
        
        k2 = lorenz_main(xtmp, force)
        xtmp = xf_old + 0.5*k2
        
        k3 = lorenz_main(xtmp, force)
        xtmp = xf_old+k3
        
        k4 = lorenz_main(xtmp, force)

        xf_new = xf_old + (k1 + 2.0 * k2 + 2.0 * k3 + k4) / 6.0 - eta_Nx
        
        NL_traj[:, tstep+1] = xf_new
        if WC:
            NL_eta_traj[:, tstep + 1] = eta
        # print("xf_new in Lorenz96 nonlinear propagation =", xf_new[0])
        xf_old = xf_new
        
    return NL_traj, NL_eta_traj

'''
# Lorenz 96 code
'''


def lorenz_main(x1, force):
    x2 = np.empty([Nx, ])
    
    x2[0] = x1[Nx-1]*(x1[1]-x1[Nx-2])-x1[0]+force
    x2[1] = x1[0]*(x1[2]-x1[Nx-1])-x1[1]+force

    for j in range(2, Nx-1):
        x2[j] = x1[j-1]*(x1[j+1]-x1[j-2])-x1[j]+force
    
    x2[Nx-1] = x1[Nx-2]*(x1[0]-x1[Nx-3])-x1[Nx-1]+force
    
    x2 = x2*dt
    
    return x2

'''
TL code
'''


def Lorenz96_TL_propogation(X_p0, Sim_length, x0, force, eta_p0, eta0, WC):
    '''
    Propogate the perturbation X_p0 using the TL of the model Sim_length
    timesteps. x0 is the full state at the initial time. X_p0 and x0 and of size Nx, eta_p0 and eta0 are of size Ne
    '''
    X_p_traj = np.empty([Nx, Sim_length+1])
    X_p_traj[:, 0] = X_p0.flat
    if WC:
        eta_p_traj = np.empty([Ne, Sim_length+1])
        eta_p_traj[:, 0] = eta_p0
        eta_Nx = eta_vector(eta0)
        eta_Nx_p = eta_vector(eta_p0)  # assumes function that converts eta to vector of length
        # Nx is linear
    else:
        eta_Nx=np.zeros((Nx,))
        eta_Nx_p=np.zeros((Nx,))
    xp_old = X_p0.flat
    xf_old = x0.flat
    
    for tstep in range(Sim_length):
        xtmp_p = xp_old
        xtmp = xf_old

        k1_p = lorenz_main_TL(xtmp, xtmp_p)
        xtmp_p = xp_old+0.5*k1_p
        
        k1 = lorenz_main(xtmp, force)
        xtmp = xf_old+0.5*k1
        
        k2_p = lorenz_main_TL(xtmp, xtmp_p)
        xtmp_p = xp_old+0.5*k2_p
        
        k2 = lorenz_main(xtmp, force)
        xtmp = xf_old+0.5*k2
        
        k3_p = lorenz_main_TL(xtmp, xtmp_p)
        xtmp_p = xp_old+k3_p
        
        k3 = lorenz_main(xtmp, force)
        xtmp = xf_old+k3
        
        k4_p = lorenz_main_TL(xtmp, xtmp_p)

        xp_new = xp_old + (k1_p + 2.0 * k2_p + 2.0 * k3_p + k4_p) / 6.0 - eta_Nx_p
        
        k4 = lorenz_main(xtmp, force)

        xf_new = xf_old + (k1 + 2.0 * k2 + 2.0 * k3+ k4) / 6.0 - eta_Nx
        
        X_p_traj[:, tstep+1] = xp_new
        if WC:
            eta_p_traj[:, tstep + 1] = eta_p0
        else:
            eta_p_traj = np.zeros([Ne, Sim_length+1])
        
        xp_old = xp_new
        xf_old = xf_new
  
    return X_p_traj, eta_p_traj

'''
# Lorenz 96 TL code
'''


def lorenz_main_TL(x1, x1_p):
    x2_p = np.empty([Nx, ])
    x2_p[0] = (x1_p[Nx-1]*(x1[1]-x1[Nx-2]) + x1[Nx-1]*(x1_p[1]-x1_p[Nx-2]) - x1_p[0])*dt
    x2_p[1] = (x1_p[0]*(x1[2]-x1[Nx-1]) + x1[0]*(x1_p[2]-x1_p[Nx-1]) - x1_p[1])*dt

    for j in range(2, Nx-1):
        x2_p[j] = (x1_p[j-1]*(x1[j+1]-x1[j-2]) + x1[j-1]*(x1_p[j+1]-x1_p[j-2]) - x1_p[j])*dt

    x2_p[Nx-1] = (x1_p[Nx-2]*(x1[0]-x1[Nx-3]) + x1[Nx-2]*(x1_p[0]-x1_p[Nx-3]) - x1_p[Nx-1])*dt
    
    return x2_p


'''
ADJ code
'''


def Lorenz96_ADJ_propogation(X_hat, X_m1, force, eta_hat, WC):
    '''
    Propogate backwards the perturbation X_hat 1 time step using the the 
    ADJOINT of the model. X_m1 is the full state at the previous timestep.
    '''
    ''' save linearisation state at intermediate steps'''
        
    k1 = lorenz_main(X_m1, force)
    xtmp1 = X_m1+0.5*k1
        
    k2 = lorenz_main(xtmp1, force)
    xtmp2 = X_m1+0.5*k2
        
    k3 = lorenz_main(xtmp2, force)
    xtmp3 = X_m1+k3
    ''' adjoint code (TL code given at each step)'''
    k4_hat = X_hat/6.0
    k3_hat = 2.*X_hat/6.0
    k2_hat = 2.*X_hat/6.0
    k1_hat = X_hat/6.0
    X0_hat = X_hat
    eta_Nx_hat = -X_hat
    X_hat = np.zeros([Nx, ])

    xtmp_hat = lorenz_main_adj(k4_hat, xtmp3)
    k4_hat = np.zeros([Nx, ])

    k3_hat = k3_hat + xtmp_hat
    X0_hat = X0_hat + xtmp_hat
    xtmp_hat = np.zeros([Nx, ])

    xtmp_hat = lorenz_main_adj(k3_hat, xtmp2)
    k3_hat = np.zeros([Nx, ])

    k2_hat = k2_hat + 0.5*xtmp_hat
    X0_hat = X0_hat + xtmp_hat
    xtmp_hat = np.zeros([Nx, ])

    xtmp_hat = lorenz_main_adj(k2_hat, xtmp1)
    k2_hat = np.zeros([Nx, ])

    k1_hat = k1_hat + 0.5*xtmp_hat
    X0_hat = X0_hat + xtmp_hat
    xtmp_hat = np.zeros([Nx, ])

    xtmp_hat = lorenz_main_adj(k1_hat, X_m1)
    k1_hat = np.zeros([Nx, ])

    X0_hat = X0_hat+xtmp_hat
    xtmp_hat = np.zeros([Nx, ])

    if WC:
        eta0_hat = eta_hat + eta_vector_ADJ(eta_Nx_hat)
    else:
        eta0_hat = [0,]
    return X0_hat, eta0_hat
    
    

'''
# Lorenz 96 ADJ code
'''
def lorenz_main_adj(x2_hat,x1):
    x1_hat=np.zeros([Nx,])

    x1_hat[Nx-2] = x1_hat[Nx-2] + (x2_hat[Nx-1]*(x1[0]-x1[Nx-3]))*dt
    x1_hat[0]    = x1_hat[0] + (x2_hat[Nx-1]*x1[Nx-2])*dt
    x1_hat[Nx-3] = x1_hat[Nx-3] - (x2_hat[Nx-1]*x1[Nx-2])*dt
    x1_hat[Nx-1] = x1_hat[Nx-1] - x2_hat[Nx-1]*dt
    x2_hat[Nx-1] = 0

    for j in range(2,Nx-1):
        x1_hat[j-1] = x1_hat[j-1] + (x2_hat[j]*(x1[j+1]-x1[j-2]))*dt
        x1_hat[j+1] = x1_hat[j+1] + (x2_hat[j]*x1[j-1])*dt
        x1_hat[j-2] = x1_hat[j-2] - (x2_hat[j]*x1[j-1])*dt
        x1_hat[j]   = x1_hat[j] - x2_hat[j]*dt
        x2_hat[j]   = 0

    x1_hat[0]    = x1_hat[0] + (x2_hat[1]*(x1[2]-x1[Nx-1]))*dt
    x1_hat[2]    = x1_hat[2] + (x2_hat[1]*x1[0])*dt
    x1_hat[Nx-1] = x1_hat[Nx-1] - (x2_hat[1]*x1[0])*dt
    x1_hat[1]    = x1_hat[1] - x2_hat[1]*dt
    x2_hat[1]    = 0

    x1_hat[Nx-1] = x1_hat[Nx-1] + (x2_hat[0]*(x1[1]-x1[Nx-2]))*dt
    x1_hat[1]    = x1_hat[1] + (x2_hat[0]*x1[Nx-1])*dt
    x1_hat[Nx-2] = x1_hat[Nx-2] - (x2_hat[0]*x1[Nx-1])*dt
    x1_hat[0]    = x1_hat[0] - x2_hat[0]*dt
    x2_hat[0]    = 0
    return x1_hat
