'''This file is not linked to the other files. It is just used to plot the bias coefficient analysis and the state
analysis from imported files.'''

import matplotlib.pyplot as plt
import pickle
import numpy as np
import parameters as p  # to be used for true trajectory only
import Lorenz96
from matplotlib.colors import TwoSlopeNorm

#  parameters
t_shift = 10  # time of later observation
half_t_shift = int(t_shift/2)  # time of earlier observation
Fa = 8.8  # model forcing (including bias)
true_b = 0.5  # observation bias
true_eta = 4*0.0125  # model bias incr*dt
WC = 1  # WC4DVar
BC = 1  # VarBC
o1 = 1  # biased observation error variance
o2 = 1  # unbiased observation error variance
L_a = 10  # window length
N_c = 20  # number of cycles
win_len = (L_a + 1)*N_c  # total analysis length
true_win_len = L_a*N_c + 1  # total time length
b_obs_sep_time = 10  # separation distance in time of biased observations
obs_varied = 0  # Are observations spatially varied?
label1 = 'When biased and anchor observations are at t=' + str(t_shift)
alpha1 = 1
label2 = 'When biased observations are at t=' + str(half_t_shift) + ' and anchor observations are at t=' +str(t_shift)
alpha2 = 0.4
label3 = 'When biased observations are at t=' + str(t_shift) + ' and anchor observations are at t=' + str(half_t_shift)
alpha3 = 0.2


def plot_state_ratio(state_ratio, alpha, label):
	"""Plot the ratio between the bias and the random error in the state."""
	plt.plot(np.arange(win_len), state_ratio, 'o-', c='r', alpha=alpha, label=label)
	plt.axhline(y=0.1, color='black', linestyle='--')
	for i in range(N_c):
		plt.axvline(x=i*L_a, color='black', linestyle='--', alpha=0.1)
	plt.ylabel("Ratio of bias to random error in state")
	# plt.xticks(size=14)
	# plt.yticks(size=14)
	plt.tight_layout()
	plt.legend(loc='lower right')


def plot_state_time(state, alpha, label, color='red'):
	"""Plot the state analysis against time"""
	for i in range(N_c):
		plt.axvline(x=i*L_a, color='black', linestyle='--', alpha=0.1)
		if i == 1:
			plt.plot(np.arange(true_win_len)[i * L_a], state[i * (L_a + 1)], 'o', color=color, alpha=alpha, label=label)
		else:
			plt.plot(np.arange(true_win_len)[i * L_a], state[i * (L_a + 1)], 'o', color=color, alpha=alpha)

		plt.plot(np.arange(true_win_len)[i * L_a:(i + 1) * L_a + 1], state[i * (L_a + 1):(i + 1) * (L_a + 1)],
				 color=color, alpha=alpha)
		plt.plot(np.arange(win_len)[i * L_a], state[i * (L_a + 1)], 'o', color=color, alpha=alpha)


def plot_obs_bias_time(bias_ave, alpha, label, btime, color):
	"""Plot the observation bias coefficient for all cycles when Nb=1"""
	# Calculate bias-corrected observation times
	b_time = np.arange(btime, true_win_len, b_obs_sep_time)
	print("b_time =", b_time)
	for i in range(np.size(b_time)):
		pt = b_time[i]
		if i == np.size(b_time) - 1:
			# print("i =", i)
			plt.plot(pt, bias_ave[0, pt - 1], 'o', color=color, alpha=alpha)
		elif i == 0:
			plt.plot(pt, bias_ave[0, pt], 'o', color=color, alpha=alpha, label=label)
		else:
			plt.plot(pt, bias_ave[0, pt], 'o', color=color, alpha=alpha)
	plt.ylabel('Mean Observation Bias Coefficient Analysis')


def plot_model_bias_time(bias_ave, alpha, label):
	"""Plot the model bias coefficient for all cycles when Ne=1"""
	for i in range(N_c):
		plt.axvline(x=i*L_a, color='black', linestyle='--', alpha=0.1)
		if i == 1:
			plt.plot(np.arange(true_win_len)[i*L_a], bias_ave[0, i*L_a], 'o', color='green', alpha=alpha, label=label)
		else:
			plt.plot(np.arange(true_win_len)[i * L_a], bias_ave[0, i*L_a], 'o', color='green', alpha=alpha)
	plt.ylabel('Mean Model Bias Parameter Analysis')


def bias_ratio():
	state_file1 = 'state_ratio_WC_' + str(WC) + '_BC_' + str(BC) + '_Fa_' + str(Fa) + '_true_b_' + str(true_b) + \
				  '_Obs_time_shift_' + str(t_shift) + '_' + str(t_shift) + '_o1_' + str(o1) + '_o2_' + str(o2)
	state_ratio1 = pickle.load(open(state_file1 + ".dat", "rb"))
	state_file2 = 'state_ratio_WC_' + str(WC) + '_BC_' + str(BC) + '_Fa_' + str(Fa) + '_true_b_' + str(true_b) + \
				  '_Obs_time_shift_' + str(half_t_shift) + '_' + str(t_shift) + '_o1_' + str(o1) + '_o2_' + str(o2)
	state_ratio2 = pickle.load(open(state_file2 + ".dat", "rb"))
	state_file3 = 'state_ratio_WC_' + str(WC) + '_BC_' + str(BC) + '_Fa_' + str(Fa) + '_true_b_' + str(true_b) + \
				  '_Obs_time_shift_' + str(t_shift) + '_' + str(half_t_shift) + '_o1_' + str(o1) + '_o2_' + str(o2)
	state_ratio3 = pickle.load(open(state_file3 + ".dat", "rb"))

	if BC:
		## obs both at t=2
		coeff_file1 = 'obs_bias_coeff_WC_' + str(WC) + '_BC_' + str(BC) + '_Fa_' + str(Fa) + '_true_b_' + str(true_b) + \
				  '_Obs_time_shift_' + str(t_shift) + '_' + str(t_shift) + '_o1_' + str(o1) + '_o2_' + str(o2)
		obs_bias_coeff1 = pickle.load(open(coeff_file1 + ".dat", "rb"))  # Mean observation bias coefficient
		coeff_std_file1 = 'obs_bias_coeff_std_WC_' + str(WC) + '_BC_' + str(BC) + '_Fa_' + str(Fa) + '_true_b_' + str(true_b) + \
				  '_Obs_time_shift_' + str(t_shift) + '_' + str(t_shift) + '_o1_' + str(o1) + '_o2_' + str(o2)
		obs_bias_std1 = pickle.load(open(coeff_std_file1 + ".dat", "rb"))  # Standard deviation of obs bias coeff
		## bias-corrected obs at t=1, anchor obs at t=2
		coeff_file2 = 'obs_bias_coeff_WC_' + str(WC) + '_BC_' + str(BC) + '_Fa_' + str(Fa) + '_true_b_' + str(true_b) + \
				  '_Obs_time_shift_' + str(half_t_shift) + '_' + str(t_shift) + '_o1_' + str(o1) + '_o2_' + str(o2)
		obs_bias_coeff2 = pickle.load(open(coeff_file2 + ".dat", "rb"))
		coeff_std_file2 = 'obs_bias_coeff_std_WC_' + str(WC) + '_BC_' + str(BC) + '_Fa_' + str(Fa) + '_true_b_' + str(true_b) + \
				  '_Obs_time_shift_' + str(half_t_shift) + '_' + str(t_shift) + '_o1_' + str(o1) + '_o2_' + str(o2)
		obs_bias_std2 = pickle.load(open(coeff_std_file2 + ".dat", "rb"))
		## bias-corrected obs at t=2, anchor obs at t=1
		coeff_file3 = 'obs_bias_coeff_WC_' + str(WC) + '_BC_' + str(BC) + '_Fa_' + str(Fa) + '_true_b_' + str(true_b) + \
				  '_Obs_time_shift_' + str(t_shift) + '_' + str(half_t_shift) + '_o1_' + str(o1) + '_o2_' + str(o2)
		obs_bias_coeff3 = pickle.load(open(coeff_file3 + ".dat", "rb"))
		coeff_std_file3 = 'obs_bias_coeff_std_WC_' + str(WC) + '_BC_' + str(BC) + '_Fa_' + str(Fa) + '_true_b_' + str(true_b) + \
				  '_Obs_time_shift_' + str(t_shift) + '_' + str(half_t_shift) + '_o1_' + str(o1) + '_o2_' + str(o2)
		obs_bias_std3 = pickle.load(open(coeff_std_file3 + ".dat", "rb"))


		## Plot the bias coefficient analysis
		plt.figure()
		plot_obs_bias_time(obs_bias_coeff1, obs_bias_std1, alpha1, label1, t_shift)
		plot_obs_bias_time(obs_bias_coeff2, obs_bias_std2, alpha2, label2, half_t_shift)
		plot_obs_bias_time(obs_bias_coeff3, obs_bias_std3, alpha3, label3, t_shift)
		plt.plot(np.arange(win_len), np.ones(win_len)*true_b, '--', color='black', label='True bias coefficient')
		plt.xlabel('Number of time steps')
		plt.xticks(np.arange(0, win_len, L_a))
		plt.legend()
		plt.show()

	# # Plot ratio between bias and random error for state, obs bias coeff and model bias coeff respectively.
	no_plots = 1  # Define number of subplots to have - depending on whether beta and eta will be plotted.
	plt.figure(1)
	plot_state_ratio(state_ratio1, alpha1, label1)
	plot_state_ratio(state_ratio2, alpha2, label2)
	plot_state_ratio(state_ratio3, alpha3, label3)
	plt.xlabel('Number of time steps')
	plt.show()


def state_analysis():
	"""Plot state analysis against time"""
	if obs_varied == 1:
		state_file1 = 'state_analysis_WC_' + str(WC) + '_BC_' + str(BC) + '_Fa_' + str(Fa) + '_true_b-varied_' + str(true_b) + \
					  '_Obs_time_shift_' + str(t_shift) + '_' + str(t_shift) + '_o1_' + str(o1) + '_o2_' + str(o2)
		state_file2 = 'state_analysis_WC_' + str(WC) + '_BC_' + str(BC) + '_Fa_' + str(Fa) + '_true_b-varied_' + str(true_b) + \
					  '_Obs_time_shift_' + str(half_t_shift) + '_' + str(t_shift) + '_o1_' + str(o1) + '_o2_' + str(o2)
		state_file3 = 'state_analysis_WC_' + str(WC) + '_BC_' + str(BC) + '_Fa_' + str(Fa) + '_true_b-varied_' + str(true_b) + \
					  '_Obs_time_shift_' + str(t_shift) + '_' + str(half_t_shift) + '_o1_' + str(o1) + '_o2_' + str(o2)
	else:
		state_file1 = 'state_analysis_WC_' + str(WC) + '_BC_' + str(BC) + '_Fa_' + str(Fa) + '_true_b_' + str(true_b) + \
					  '_Obs_time_shift_' + str(t_shift) + '_' + str(t_shift) + '_o1_' + str(o1) + '_o2_' + str(o2)
		obs_file1 = 'obs_WC_' + str(WC) + '_BC_' + str(BC) + '_Fa_' + str(Fa) + '_true_b_' + str(true_b) + \
				 '_Obs_time_shift_' + str(t_shift) + '_' + str(t_shift) + '_o1_' + str(o1) + '_o2_' + str(o2)
		state_file2 = 'state_analysis_WC_' + str(WC) + '_BC_' + str(BC) + '_Fa_' + str(Fa) + '_true_b_' + str(true_b) + \
					  '_Obs_time_shift_' + str(half_t_shift) + '_' + str(t_shift) + '_o1_' + str(o1) + '_o2_' + str(o2)
		state_file3 = 'state_analysis_WC_' + str(WC) + '_BC_' + str(BC) + '_Fa_' + str(Fa) + '_true_b_' + str(true_b) + \
					  '_Obs_time_shift_' + str(t_shift) + '_' + str(half_t_shift) + '_o1_' + str(o1) + '_o2_' + str(o2)
	state_analysis1 = pickle.load(open(state_file1 + ".dat", "rb"))
	obs1 = pickle.load(open(obs_file1 + ".dat", "rb"))
	y1 = obs1[0]
	y2 = obs1[1]
	state_analysis2 = pickle.load(open(state_file2 + ".dat", "rb"))
	state_analysis3 = pickle.load(open(state_file3 + ".dat", "rb"))
	true = Lorenz96.Lorenz96_nonLinear_propogation(p.Ics(), true_win_len, p.F)[0]  # Calculate true trajectory
	forced = Lorenz96.Lorenz96_nonLinear_propogation(p.Ics(), true_win_len, Fa)[0]  # Calculate forced trajectory (free
	# run)
	mean_background_forced = np.zeros(win_len)
	background_forced = np.zeros((40, win_len))
	mean_true = np.zeros(true_win_len)
	mean_forced = np.zeros(true_win_len)
	forced_diff = np.zeros((40, true_win_len))
	mean_forced_diff = np.zeros(true_win_len)
	mean_realobs1 = np.zeros((40, N_c))
	mean_realobs2 = np.zeros((40, N_c))
	mean_realisation1 = np.zeros((40, win_len))
	real_diff1 = np.zeros((40, win_len))
	mean_obs1 = np.zeros(N_c)
	mean_obs2 = np.zeros(N_c)
	mean_state1 = np.zeros(win_len)
	mean_diff1 = np.zeros(win_len)
	mean_realisation2 = np.zeros((40, win_len))
	real_diff2 = np.zeros((40, win_len))
	mean_state2 = np.zeros(win_len)
	mean_diff2 = np.zeros(win_len)
	mean_realisation3 = np.zeros((40, win_len))
	real_diff3 = np.zeros((40, win_len))
	mean_state3 = np.zeros(win_len)
	mean_diff3 = np.zeros(win_len)
	for t in range(N_c):
		for i in range(40):
			mean_realobs1[i, t] = np.mean(y1[:, i, t])
			mean_realobs2[i, t] = np.mean(y2[:, i, t])

	for t in range(win_len):
		for i in range(40):
			# Calculate mean of all realisations
			mean_realisation1[i, t] = np.mean(state_analysis1[:, i, t])
			mean_realisation2[i, t] = np.mean(state_analysis2[:, i, t])
			mean_realisation3[i, t] = np.mean(state_analysis3[:, i, t])
			if t < true_win_len:
				# Calculate difference between forced traj and true traj
				forced_diff[i, t] = np.subtract(forced[i, t], true[i, t])
	# Calculate difference between mean realisation and truth - there will be two values of analysis
	# for the end and beginning of each window from the previous and current cycle, so mean-real and
	# truth have different time indexes to give the same time.
	k = 0
	for N in range(N_c):
		background_forced[:, N*L_a + k: (N+1)*L_a + 1 + k] = Lorenz96.Lorenz96_nonLinear_propogation(
				mean_realisation1[:, N*L_a + k], L_a, Fa)[0]
		mean_obs1[N] = np.mean(mean_realobs1[:, N])
		mean_obs2[N] = np.mean(mean_realobs2[:, N])
		for t in range(L_a + 1):
			# print("state time:", N*L_a + t + k, "true time:", N*L_a + t)
			real_diff1[:, N*L_a + t + k] = np.subtract(mean_realisation1[:, N*L_a + t + k], true[:, N*L_a + t])
			real_diff2[:, N*L_a + t + k] = np.subtract(mean_realisation2[:, N*L_a + t + k], true[:, N*L_a + t])
			real_diff3[:, N*L_a + t + k] = np.subtract(mean_realisation3[:, N*L_a + t + k], true[:, N*L_a + t])

		k += 1
	for t in range(win_len):
		# Calculate mean of all states
		mean_state1[t] = np.mean(mean_realisation1[:, t])
		mean_state2[t] = np.mean(mean_realisation2[:, t])
		mean_state3[t] = np.mean(mean_realisation3[:, t])
		# Calculate mean difference across all states
		mean_diff1[t] = np.mean(real_diff1[:, t])
		mean_diff2[t] = np.mean(real_diff2[:, t])
		mean_diff3[t] = np.mean(real_diff3[:, t])
		mean_background_forced[t] = np.mean(background_forced[:, t])
		if t < true_win_len:
			# Calculate mean of true trajectory across all states
			mean_true[t] = np.mean(true[:, t])
			# Calculate mean of forced free run trajectory across all states
			mean_forced[t] = np.mean(forced[:, t])
			# Calculate mean difference between forced and true across all states
			mean_forced_diff[t] = np.mean(forced_diff[:, t])

	# Calculate mean of state analysis bias across all analysis times to give scalar value for each experiment.
	diff_times1 = []
	diff_times2 = []
	diff_times3 = []
	for i in range(N_c):
		diff_times1 = np.append(diff_times1, mean_diff1[i * (L_a + 1)])  # store analysis biases at analysis times in
		# an array
		diff_times2 = np.append(diff_times2, mean_diff2[i * (L_a + 1)])
		diff_times3 = np.append(diff_times3, mean_diff3[i * (L_a + 1)])
		traj_diff = mean_state1[(i + 1) * L_a + 1] - mean_state1[i * L_a]
	print("traj_diff =", traj_diff)
	mean_time1 = np.mean(diff_times1)
	mean_time2 = np.mean(diff_times2)
	mean_time3 = np.mean(diff_times3)
	print("mean state analysis bias over all states and time when t1=t2 =", mean_time1)
	print("mean state analysis bias over all states and time when t1<t2 =", mean_time2)
	print("mean state analysis bias over all states and time when t1>t2 =", mean_time3)

	# Plot true mean against state analysis mean
	plt.figure()
	plt.plot(np.arange(true_win_len), mean_true, '--', color='black', label='Mean truth')
	# for i in range(N_c):
		# plt.plot(np.arange(true_win_len)[int(i*L_a + 2)], mean_obs1[i], '*', color='purple', label='Biased obs')
		# plt.plot(np.arange(true_win_len)[int(i*L_a + 2)], mean_obs2[i], 'x', color='orange', label='Unbiased obs')
	# plt.plot(np.arange(true_win_len), mean_forced, ':', color='blue', label='Mean free run')
	# plot_state_time(mean_background_forced, 1, "Mean background trajectory", color='blue')
	plot_state_time(mean_state1, alpha1, label1)
	plot_state_time(mean_state2, alpha2, label2)
	plot_state_time(mean_state3, alpha3, label3)
	plt.xlabel('Number of time steps', size=15)
	plt.ylabel('Mean state', size=15)
	plt.legend(fontsize=15)
	plt.show()

	# Plot mean difference between truth and state analysis
	plt.figure()
	plot_state_time(mean_diff1, alpha1, label1)
	plot_state_time(mean_diff2, alpha2, label2)
	plot_state_time(mean_diff3, alpha3, label3)
	# plt.plot(np.arange(true_win_len), mean_forced_diff, ':', color='blue', label='free run')
	plt.xlabel('Number of time steps')
	plt.ylabel('Mean difference between analysis and truth')
	plt.legend()
	plt.show()


def obs_bias_analysis():
	"""Plot bias analysis against time"""
	if obs_varied == 1:
		## obs both at t=2
		coeff_file1 = 'obs_bias_coeff_WC_' + str(WC) + '_BC_' + str(BC) + '_Fa_' + str(Fa) + '_true_b_' + str(true_b) + \
					  '_Obs_time_shift_' + str(t_shift) + '_' + str(t_shift) + '_o1_' + str(o1) + '_o2_' + str(o2)
		## bias-corrected obs at t=1, anchor obs at t=2
		coeff_file2 = 'obs_bias_coeff_WC_' + str(WC) + '_BC_' + str(BC) + '_Fa_' + str(Fa) + '_true_b_' + str(true_b) + \
					  '_Obs_time_shift_' + str(half_t_shift) + '_' + str(t_shift) + '_o1_' + str(o1) + '_o2_' + str(o2)
		## bias-corrected obs at t=2, anchor obs at t=1
		coeff_file3 = 'obs_bias_coeff_WC_' + str(WC) + '_BC_' + str(BC) + '_Fa_' + str(Fa) + '_true_b_' + str(true_b) + \
					  '_Obs_time_shift_' + str(t_shift) + '_' + str(half_t_shift) + '_o1_' + str(o1) + '_o2_' + str(o2)
	else:
		## obs both at t=2
		coeff_file1 = 'obs_bias_coeff_WC_' + str(WC) + '_BC_' + str(BC) + '_Fa_' + str(Fa) + '_true_b_' + str(true_b) + \
					  '_Obs_time_shift_' + str(t_shift) + '_' + str(t_shift) + '_o1_' + str(o1) + '_o2_' + str(o2)
		## bias-corrected obs at t=1, anchor obs at t=2
		coeff_file2 = 'obs_bias_coeff_WC_' + str(WC) + '_BC_' + str(BC) + '_Fa_' + str(Fa) + '_true_b_' + str(true_b) + \
					  '_Obs_time_shift_' + str(half_t_shift) + '_' + str(t_shift) + '_o1_' + str(o1) + '_o2_' + str(o2)
		## bias-corrected obs at t=2, anchor obs at t=1
		coeff_file3 = 'obs_bias_coeff_WC_' + str(WC) + '_BC_' + str(BC) + '_Fa_' + str(Fa) + '_true_b_' + str(true_b) + \
					  '_Obs_time_shift_' + str(t_shift) + '_' + str(half_t_shift) + '_o1_' + str(o1) + '_o2_' + str(o2)
	obs_bias_coeff1 = pickle.load(open(coeff_file1 + ".dat", "rb"))
	obs_bias_coeff2 = pickle.load(open(coeff_file2 + ".dat", "rb"))
	obs_bias_coeff3 = pickle.load(open(coeff_file3 + ".dat", "rb"))

	# Calculate mean of bias coefficient analysis bias across all analysis times to give scalar value for each
	# experiment.
	diff1 = np.subtract(obs_bias_coeff1, true_b)
	diff2 = np.subtract(obs_bias_coeff2, true_b)
	diff3 = np.subtract(obs_bias_coeff3, true_b)
	diff_times1 = []
	diff_times2 = []
	diff_times3 = []
	for i in range(N_c):
		diff_times1 = np.append(diff_times1, diff1[0, i * L_a])  # store analysis biases at analysis times in
		# an array
		diff_times2 = np.append(diff_times2, diff2[0, i * L_a])
		diff_times3 = np.append(diff_times3, diff3[0, i * L_a])
	mean_time1 = np.mean(diff_times1)
	mean_time2 = np.mean(diff_times2)
	mean_time3 = np.mean(diff_times3)
	print("mean obs analysis bias over all states and time when t1=t2 =", mean_time1)
	print("mean obs analysis bias over all states and time when t1<t2 =", mean_time2)
	print("mean obs analysis bias over all states and time when t1>t2 =", mean_time3)

	# Plot the bias coefficient analysis
	color = 'purple'
	plt.figure()
	plot_obs_bias_time(obs_bias_coeff1, alpha1, label1, t_shift, color)
	plot_obs_bias_time(obs_bias_coeff2, alpha2, label2, half_t_shift, color)
	plot_obs_bias_time(obs_bias_coeff3, alpha3, label3, t_shift, color)
	plt.plot(np.arange(true_win_len), np.ones(true_win_len) * true_b, '--', color='black', label='True observation bias coefficient')
	for i in range(N_c):
		plt.axvline(x=i*L_a, color='black', linestyle='--', alpha=0.1)
	plt.xlabel('Number of time steps')
	# plt.xticks(np.arange(0, true_win_len, L_a))
	plt.legend(loc='upper right')
	plt.show()


def model_bias_analysis():
	"""Plot bias analysis against time"""
	if obs_varied == 1:
		## obs both at t=2
		coeff_file1 = 'model_bias_coeff_WC_' + str(WC) + '_BC_' + str(BC) + '_Fa_' + str(Fa) + '_true_b-varied_' + str(
			true_b) + \
					  '_Obs_time_shift_' + str(t_shift) + '_' + str(t_shift) + '_o1_' + str(o1) + '_o2_' + str(o2)
		## bias-corrected obs at t=1, anchor obs at t=2
		coeff_file2 = 'model_bias_coeff_WC_' + str(WC) + '_BC_' + str(BC) + '_Fa_' + str(Fa) + '_true_b-varied_' + str(
			true_b) + \
					  '_Obs_time_shift_' + str(half_t_shift) + '_' + str(t_shift) + '_o1_' + str(o1) + '_o2_' + str(o2)
		## bias-corrected obs at t=2, anchor obs at t=1
		coeff_file3 = 'model_bias_coeff_WC_' + str(WC) + '_BC_' + str(BC) + '_Fa_' + str(Fa) + '_true_b-varied_' + str(
			true_b) + \
					  '_Obs_time_shift_' + str(t_shift) + '_' + str(half_t_shift) + '_o1_' + str(o1) + '_o2_' + str(o2)
	else:
		## obs both at t=2
		coeff_file1 = 'model_bias_coeff_WC_' + str(WC) + '_BC_' + str(BC) + '_Fa_' + str(Fa) + '_true_b_' + str(true_b) + \
					  '_Obs_time_shift_' + str(t_shift) + '_' + str(t_shift) + '_o1_' + str(o1) + '_o2_' + str(o2)
		## bias-corrected obs at t=1, anchor obs at t=2
		coeff_file2 = 'model_bias_coeff_WC_' + str(WC) + '_BC_' + str(BC) + '_Fa_' + str(Fa) + '_true_b_' + str(true_b) + \
					  '_Obs_time_shift_' + str(half_t_shift) + '_' + str(t_shift) + '_o1_' + str(o1) + '_o2_' + str(o2)
		## bias-corrected obs at t=2, anchor obs at t=1
		coeff_file3 = 'model_bias_coeff_WC_' + str(WC) + '_BC_' + str(BC) + '_Fa_' + str(Fa) + '_true_b_' + str(true_b) + \
					  '_Obs_time_shift_' + str(t_shift) + '_' + str(half_t_shift) + '_o1_' + str(o1) + '_o2_' + str(o2)
	model_bias_coeff1 = pickle.load(open(coeff_file1 + ".dat", "rb"))  # Mean observation bias coefficient
	model_bias_coeff2 = pickle.load(open(coeff_file2 + ".dat", "rb"))
	model_bias_coeff3 = pickle.load(open(coeff_file3 + ".dat", "rb"))

	# Calculate mean of bias coefficient analysis bias across all analysis times to give scalar value for each
	# experiment.
	diff1 = np.subtract(model_bias_coeff1, true_eta)
	diff2 = np.subtract(model_bias_coeff2, true_eta)
	diff3 = np.subtract(model_bias_coeff3, true_eta)
	diff_times1 = []
	diff_times2 = []
	diff_times3 = []
	for i in range(N_c):
		diff_times1 = np.append(diff_times1, diff1[0, i * L_a])  # store analysis biases at analysis times in
		# an array
		diff_times2 = np.append(diff_times2, diff2[0, i * L_a])
		diff_times3 = np.append(diff_times3, diff3[0, i * L_a])
	mean_time1 = np.mean(diff_times1)
	mean_time2 = np.mean(diff_times2)
	mean_time3 = np.mean(diff_times3)
	print("mean model analysis bias over all states and time when t1=t2 =", mean_time1)
	print("mean model analysis bias over all states and time when t1<t2 =", mean_time2)
	print("mean model analysis bias over all states and time when t1>t2 =", mean_time3)

	## Plot the bias coefficient analysis
	color = 'orange'
	plt.figure()
	plot_model_bias_time(model_bias_coeff1, alpha1, label1)
	plot_model_bias_time(model_bias_coeff2, alpha2, label2)
	plot_model_bias_time(model_bias_coeff3, alpha3, label3)
	plt.plot(np.arange(true_win_len), np.ones(true_win_len) * true_eta, '--', color='black', label='True model bias parameter')
	for i in range(N_c):
		plt.axvline(x=i*L_a, color='black', linestyle='--', alpha=0.1)
	plt.xlabel('Number of time steps')
	plt.xticks(np.arange(0, true_win_len, L_a))
	plt.legend()
	plt.show()


def plot_Bx():
	"""Plot the state background error covariance matrix from the saved file. """
	B = pickle.load(open("B_Fa8.8_obs-dist1_La-10_Lb-1_c-700_iter-1.dat", "rb")) # B_Fa_8.8_Nx_40_WC_0_BC_1_Nobtypes_2_Obs_sep_dist_1_1_La_10_Lf_1_dt_0.0125_o1_1_o2_1_ob_1_obb_0.5.dat
	norm = TwoSlopeNorm(vmin=-B.max(), vcenter=0, vmax=B.max())
	im = plt.imshow(B[0:p.Nx, 0:p.Nx], norm=norm, origin='upper', cmap='seismic')
	cb = plt.colorbar(im)
	cb.set_label(label='$B_x$', size='xx-large')
	plt.xticks(np.arange(0, p.Nx), size=10)
	plt.yticks(np.arange(0, p.Nx), size=10)
	plt.xlabel('States: $x_0$ - $x_{39}$', fontsize=20)
	plt.ylabel('States: $x_0$ - $x_{39}$', fontsize=20)
	plt.show()

state_analysis()
obs_bias_analysis()
model_bias_analysis()
# plot_Bx()

