In parameters.py file: Define all state, model bias parameter and observation bias coefficient parameters. 
In main.py file: Choose number of realisations, the bias correction techniques and the observation error variances. 
Run main.py to save files with all realisations of state and bias coefficients for each setup. 
Run plots.py to plot analysis-time plots. NOTE: In plots.py, parameters need to be redefined as multiple set ups are used at once. 
